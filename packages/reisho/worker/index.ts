import { precacheAndRoute } from "workbox-precaching"

//
;(self as any).__WB_DISABLE_DEV_LOGS = false

self.addEventListener("message", async (e) => {
  if (e.data?.action === "CACHE_NEW_ROUTE") {
    const cache = await caches.open("others")
    const url = ((e.source as unknown) as { url: string }).url
    const res = await cache.match(url)

    if (res === undefined) {
      return cache.add(url)
    }
  }
})

precacheAndRoute(
  [
    {
      url: "/",
      revision: "20210308",
    },
    {
      url: "/settings",
      revision: "20210308",
    },
  ],
  {
    ignoreURLParametersMatching: [/^q$/],
  }
)

export {}
