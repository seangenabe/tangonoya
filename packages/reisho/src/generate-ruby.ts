export function generateRuby(
  kanji: string,
  reading: string
): (string | Ruby)[] {
  const ret: (string | Ruby)[] = []

  const kanjiChars = Array.from(kanji)
  const readingChars = Array.from(reading)

  const commitAtIndex = (kanjiIndex: number, readingIndex: number) => {
    const remainingKanji = kanjiChars.slice(kanjiIndex + 1).join("")
    const remainingReading = readingChars.slice(readingIndex + 1).join("")

    if (remainingKanji || remainingReading) {
      ret.unshift({
        body: remainingKanji,
        reading: remainingReading,
      })
    }

    ret.unshift(
      kanjiChars[kanjiIndex] // === readingChars[readingIndex]
    )

    kanjiChars.splice(kanjiIndex)
    readingChars.splice(readingIndex)
  }

  d: do {
    let simpleLookbehind = false
    for (
      let readingIndex = readingChars.length - 1;
      readingIndex >= 0;
      readingIndex--
    ) {
      for (
        let kanjiIndex = kanjiChars.length - 1;
        kanjiIndex >= 0;
        kanjiIndex--
      ) {
        if (kanjiChars[kanjiIndex] === readingChars[readingIndex]) {
          commitAtIndex(kanjiIndex, readingIndex)
          continue d
        }
        if (!simpleLookbehind) {
          for (
            let newKanjiIndex = kanjiIndex - 1;
            newKanjiIndex >= 0 && newKanjiIndex >= kanjiIndex - 2;
            newKanjiIndex--
          ) {
            for (
              let newReadingIndex = readingIndex - 1;
              newReadingIndex >= 0 && newReadingIndex >= readingIndex - 5;
              newReadingIndex--
            ) {
              if (kanjiChars[newKanjiIndex] === readingChars[newReadingIndex]) {
                commitAtIndex(newKanjiIndex, newReadingIndex)
                continue d
              }
            }
          }

          simpleLookbehind = true
        }
      }
    }

    ret.unshift({ body: kanjiChars.join(""), reading: readingChars.join("") })
    kanjiChars.splice(0)
    readingChars.splice(0)
  } while (readingChars.length > 0)

  return ret
}

export interface Ruby {
  body: string
  reading: string
}
