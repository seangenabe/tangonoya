import { Jmdict, Sense } from "@tangonoya/provider-jmdict/types.gen"
import { Entry } from "@tangonoya/provider-wanikani/types.gen"
import { DBSchema } from "idb"
import { deleteDB, openDB } from "idb/with-async-ittr-cjs"
import { JmdictH } from "./jmdict"

export async function openDictionaryDb() {
  const db = await openDB<DbSchema>("dictionary", 1, {
    upgrade(db, oldVersion, newVersion, transaction) {
      // jmdict
      db.createObjectStore("jmdict", {
        keyPath: "ent_seq",
      })

      const jmdictH = db.createObjectStore("jmdict_h", {
        keyPath: "id",
      })
      jmdictH.createIndex("h", "h")
      jmdictH.createIndex("e", "e")

      db.createObjectStore("jmdict_senses", {
        keyPath: "id",
      }).createIndex("ent_seq", "ent_seq")

      // wanikani
      db.createObjectStore("wanikani", {
        keyPath: "id",
      })
      db.createObjectStore("wanikani_h", {
        keyPath: "id",
      }).createIndex("h", "h")

      // meta
      db.createObjectStore("meta")
    },
  })
  return db
}

export async function deleteDictionaryDb() {
  await deleteDB("dictionary")
}

export type JmdictReduced = Omit<Jmdict, "sense">
export interface JmdictIndexedSense {
  /** ent_seq.senseIndex */
  id: string
  ent_seq: number
  sense: Sense
}

export interface DbSchema extends DBSchema {
  jmdict: {
    key: number
    value: JmdictReduced & { mdate: string }
    indexes: {
      ent_seq: number
    }
  }
  jmdict_h: {
    key: string
    value: JmdictH
    indexes: {
      h: string
      e: number
    }
  }
  jmdict_senses: {
    key: string
    value: JmdictIndexedSense
    indexes: { ent_seq: number }
  }
  wanikani: {
    key: number
    value: Entry
    indexes: {
      id: number
    }
  }
  wanikani_h: {
    key: string
    value: never
    indexes: {
      h: string
    }
  }
  meta: {
    key: "wanikani" | "jmdict"
    value: unknown
  }
}
