declare module "tiny-segmenter" {
  class TinySegmenter {
    private patterns: Record<string, string>
    private ctype_(str: string): string
    private ts_<T>(v: T): T | "0"
    segment(input?: string | null): string[]
  }

  export = TinySegmenter
}
