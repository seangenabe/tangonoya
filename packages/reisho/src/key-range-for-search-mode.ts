export function keyRangeForSearchMode(
  q: string,
  searchMode: "exact" | "prefix"
) {
  if (searchMode === "exact") {
    return IDBKeyRange.only(q)
  } else if (searchMode === "prefix") {
    return IDBKeyRange.bound(
      q,
      `${q.slice(0, q.length - 1)}${String.fromCharCode(
        q.charCodeAt(q.length - 1) + 1
      )}`,
      true,
      true
    )
  }
}
