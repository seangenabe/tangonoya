import { useRouter } from "next/router"
import { useCallback } from "react"

export function useQuery() {
  const router = useRouter()
  const { q = "" } = router.query as { q: string }

  const setQ = useCallback(
    (q: string) => {
      router.push(q ? { pathname: "/", query: { q } } : "/", undefined, {
        shallow: true,
      })
    },
    [router]
  )

  return [q, setQ] as const
}
