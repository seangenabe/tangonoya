import {
  JmdictPatch,
  JmdictSenseByLangEntry,
  JmdictMetaJson,
} from "@tangonoya/provider-jmdict"
import { Jmdict, Sense } from "@tangonoya/provider-jmdict/types.gen"
import { ok } from "assert"
import readerAsIterator from "fetch-ndjson"
import { IDBPObjectStore, StoreNames } from "idb"
import { from } from "ix/asynciterable"
import { buffer, map } from "ix/asynciterable/operators"
import ky from "ky"
import { DbSchema, JmdictIndexedSense, openDictionaryDb } from "./dictionary"
import { JmdictDictionaryDownloaderContextObject } from "./dictionary-downloader-context"
import { keyRangeForSearchMode } from "./key-range-for-search-mode"
import { applyOperation } from "fast-json-patch"

const REPORT_INTERVAL = 100
const BUFFER_ITEM_COUNT = 1000

export async function* getJmdictEntries({
  q,
  signal,
  searchMode,
  dictionaryDownloader,
}: {
  q: string
  signal: AbortSignal
  searchMode: "exact" | "prefix" | "gloss"
  dictionaryDownloader: JmdictDictionaryDownloaderContextObject
}) {
  if (navigator.onLine) {
    // Online
    yield* getEntriesForQueryOnline(q, searchMode, signal)
    return
  }
  if (!dictionaryDownloader.mdate) {
    return
  }
  // Offline
  yield* getEntriesForQueryOffline(q, searchMode)
}

export async function checkUpdateNeeded({ signal }: { signal?: AbortSignal }) {
  const db = await openDictionaryDb()
  const { mdate: oldMdate } =
    ((await db.get("meta", "jmdict")) as { mdate: string } | undefined) ?? {}

  if (!oldMdate) {
    return true
  }

  const { mdate: newMdate }: JmdictMetaJson = await ky
    .get("/dict/jmdict/meta.json")
    .json()

  return newMdate > oldMdate
}

export async function downloadDictionaryData({
  signal,
  progress = () => undefined,
}: {
  signal?: AbortSignal
  progress?: (i: number) => void
}) {
  const { mdate }: JmdictMetaJson = await ky
    .get("/dict/jmdict/meta.json")
    .json()
  const entriesResponse = await ky.get("/dict/jmdict/entries.ndjson", {
    signal,
    headers: {
      accept: "application/x-ndjson",
    },
  })

  ok(entriesResponse.body)

  const entriesReader = entriesResponse.body.getReader()
  const entriesIterator = readerAsIterator(
    entriesReader
  ) as AsyncGenerator<Jmdict>
  const db = await openDictionaryDb()

  let i = 0

  await readAll(
    from(entriesIterator).pipe(
      buffer(BUFFER_ITEM_COUNT),
      map(async (entries): Promise<void> => {
        const t = db.transaction(["jmdict", "jmdict_h"], "readwrite")
        const jmdict = t.objectStore("jmdict")
        const h = t.objectStore("jmdict_h")

        for (const entry of entries) {
          const { _id, ...rest } = entry as Jmdict & { _id: number }

          if (i % REPORT_INTERVAL === 0) {
            progress(i)
          }
          i++

          await addNewJmdictEntry({ jmdict, h, entry: { ...entry, mdate } })
        }
      })
    )
  )

  await downloadSenseData({ signal })

  await db.put("meta", { mdate }, "jmdict")
}

export async function updateDictionaryData({
  signal,
}: {
  signal?: AbortSignal
}) {
  const db = await openDictionaryDb()
  const { mdate } = (await db.get("meta", "jmdict")) as { mdate: string }

  const patches: JmdictPatch[] = await ky
    .get("/jmdict/patches", {
      signal,
      searchParams: {
        after: mdate,
      },
    })
    .json()

  await readAll(
    from(patches).pipe(
      buffer(BUFFER_ITEM_COUNT),
      map<JmdictPatch[], void>(async (patchesBatch): Promise<void> => {
        const t = db.transaction(
          ["jmdict", "jmdict_h", "jmdict_senses"],
          "readwrite"
        )

        const jmdict = t.objectStore("jmdict")
        const h = t.objectStore("jmdict_h")
        const sensesStore = t.objectStore("jmdict_senses")

        await Promise.all(
          patchesBatch.map(
            async ({
              mdate,
              ent_seq,
              operations,
              newDocument,
            }): Promise<void> => {
              if (newDocument) {
                await addNewJmdictEntry({
                  jmdict,
                  h,
                  senses: sensesStore,
                  entry: { ...newDocument, mdate },
                })

                return
              }

              const entryReduced = await jmdict.get(ent_seq)
              ok(entryReduced)

              if (entryReduced.mdate === mdate) {
                return
              }

              const entrySenses: JmdictIndexedSense[] =
                await sensesStore.getAll(
                  IDBKeyRange.bound(`${ent_seq}.0`, `${ent_seq}/`, false, true)
                )
              const senses: Sense[] = []
              for (const { id, sense } of entrySenses) {
                const [, senseIndexStr] = id.split(".")
                const senseIndex = Number(senseIndexStr)
                senses[senseIndex] = sense
              }

              const entry: Jmdict & { mdate: string } = {
                ...entryReduced,
                sense: senses,
                mdate,
              }

              // Apply operations
              // TODO: Filter operations by used languages
              for (const operation of operations ?? []) {
                applyOperation(entry, operation)
              }

              // Save entry
              await updateJmdictEntry({ jmdict, h, senses: sensesStore, entry })
            }
          )
        )
      })
    )
  )
}

async function downloadSenseData({ signal }: { signal?: AbortSignal }) {
  const entriesResponse = await ky.get("/dict/jmdict/entries-eng.ndjson", {
    signal,
    headers: { accept: "application/x-ndjson" },
  })

  ok(entriesResponse.body)

  const entriesReader = entriesResponse.body.getReader()
  const entriesIterator = readerAsIterator(
    entriesReader
  ) as AsyncGenerator<JmdictSenseByLangEntry>
  const db = await openDictionaryDb()

  await readAll(
    from(entriesIterator).pipe(
      buffer(BUFFER_ITEM_COUNT),
      map((entries) => {
        const t = db.transaction(["jmdict_senses"], "readwrite")
        const sensesStore = t.objectStore("jmdict_senses")

        return Promise.all(
          entries.flatMap(({ ent_seq, senses }) =>
            senses.map(async ([senseIndex, sense]) => {
              return sensesStore.put({
                id: `${ent_seq}.${senseIndex}`,
                ent_seq,
                sense,
              })
            })
          )
        )
      })
    )
  )
}

export async function* getEntriesForQueryOnline(
  q: string,
  searchMode: "exact" | "prefix" | "gloss",
  signal?: AbortSignal
): AsyncGenerator<Jmdict> {
  if (q.trim() === "") {
    return
  }

  let response: Response

  if (searchMode === "gloss") {
    response = await ky.get("jmdict/search-glosses", {
      prefixUrl: process.env.NEXT_PUBLIC_BACKEND_URL,
      searchParams: {
        q,
        lang: "eng",
      },
      signal,
      headers: {
        accept: "application/x-ndjson",
      },
    })
  } else {
    response = await ky.get("jmdict/search", {
      prefixUrl: process.env.NEXT_PUBLIC_BACKEND_URL,
      searchParams: {
        q,
        search_mode: searchMode,
      },
      signal,
      headers: {
        accept: "application/x-ndjson",
      },
    })
  }

  ok(response.body)
  const reader = response.body.getReader()
  const iterator = readerAsIterator(reader)

  yield* iterator
}

export async function* getEntriesForQueryOffline(
  q: string,
  searchMode: "exact" | "prefix" | "gloss"
) {
  if (q.trim() === "") {
    return
  }

  const db = await openDictionaryDb()
  const h = db.transaction("jmdict_h", "readonly")
  const seen = new Set<number>()

  let indexItems: JmdictH[] = []
  if (searchMode === "gloss") {
  } else {
    indexItems = await h.store
      .index("h")
      .getAll(keyRangeForSearchMode(q, searchMode))
  }

  for (const { e } of indexItems) {
    if (seen.has(e)) {
      continue
    }

    const entry = await db.transaction("jmdict").store.get(e)
    ok(entry)
    const indexedSenses: JmdictIndexedSense[] = await db
      .transaction("jmdict_senses")
      .store.index("ent_seq")
      .getAll(e)

    yield { ...entry, sense: indexedSenses.map((s) => s.sense) }

    seen.add(e)
  }
}

export async function* getJmdictGlossResults({
  q,
  signal,
  dictionaryDownloader,
}: {
  q: string
  signal: AbortSignal
  dictionaryDownloader: JmdictDictionaryDownloaderContextObject
}) {
  if (dictionaryDownloader.mdate == null) {
    return
  }
  if (dictionaryDownloader.mdate === "") {
    // Online
    yield* getGlossResultsForQueryOnline(q, signal)
  } else {
    // Offline
    yield* getGlossResultsForQueryOffline(q, signal)
  }
}

export async function* getGlossResultsForQueryOnline(
  q: string,
  signal: AbortSignal
) {
  if (q.trim() === "") {
    return
  }
  const response = await ky.get("jmdict/search-glosses", {
    prefixUrl: process.env.NEXT_PUBLIC_BACKEND_URL,
    searchParams: {
      q,
      lang: "eng",
    },
    signal,
    headers: {
      accept: "application/x-ndjson",
    },
  })

  ok(response.body)
  const reader = response.body.getReader()
  const iterator = readerAsIterator(reader)

  yield* iterator
}

async function readAll(source: AsyncIterable<unknown>) {
  for await (const _ of source) {
  }
}

type MyObjectStore<
  T extends StoreNames<DbSchema>,
  Stores extends StoreNames<DbSchema>[] = StoreNames<DbSchema>[]
> = IDBPObjectStore<DbSchema, Stores, T, "readwrite">

async function addNewJmdictEntry<
  JmdictStores extends StoreNames<DbSchema>[] = "jmdict"[],
  HStores extends StoreNames<DbSchema>[] = "jmdict_h"[],
  SenseStores extends StoreNames<DbSchema>[] = "jmdict_senses"[]
>({
  jmdict,
  h,
  senses,
  entry,
}: {
  jmdict: MyObjectStore<"jmdict", JmdictStores>
  h: MyObjectStore<"jmdict_h", HStores>
  senses?: MyObjectStore<"jmdict_senses", SenseStores>
  entry: Jmdict & { sense?: Jmdict["sense"]; mdate: string }
}) {
  ok(!("_id" in entry))

  // Create entry
  const { sense: _, ...rest } = entry
  await jmdict.put(rest)

  await createHeadwordEntries({ h, entry })

  // Create sense entries
  if (senses && entry.sense) {
    await createSenseEntries({ senses, entry })
  }
}

/**
 * Create entries for headwords
 */
async function createHeadwordEntries<
  HStores extends StoreNames<DbSchema>[] = "jmdict_h"[]
>({
  h,
  entry,
}: {
  h: MyObjectStore<"jmdict_h", HStores>
  entry: Jmdict & { sense?: Jmdict["sense"]; mdate: string }
}) {
  for (const k_ele of entry.k_ele ?? []) {
    await h.put({
      id: `${k_ele.keb}!${entry.ent_seq}`,
      e: entry.ent_seq,
      h: k_ele.keb,
    })
  }

  for (const r_ele of entry.r_ele ?? []) {
    await h.put({
      id: `${r_ele.reb}!${entry.ent_seq}`,
      e: entry.ent_seq,
      h: r_ele.reb,
    })
  }
}

/**
 * Create sense entries
 */
async function createSenseEntries<
  SenseStores extends StoreNames<DbSchema>[] = "jmdict_senses"[]
>({
  senses,
  entry,
}: {
  senses: MyObjectStore<"jmdict_senses", SenseStores>
  entry: Pick<Jmdict, "ent_seq" | "sense">
}) {
  // TODO: Filter unused languages
  for (const [senseIndex, senseValue] of entry.sense.entries()) {
    await senses.put({
      id: `${entry.ent_seq}.${senseIndex}`,
      ent_seq: entry.ent_seq,
      sense: senseValue,
    })
  }
}

async function updateJmdictEntry<
  JmdictStores extends StoreNames<DbSchema>[] = "jmdict"[],
  HStores extends StoreNames<DbSchema>[] = "jmdict_h"[],
  SenseStores extends StoreNames<DbSchema>[] = "jmdict_senses"[]
>({
  jmdict,
  h,
  senses,
  entry,
}: {
  jmdict: MyObjectStore<"jmdict", JmdictStores>
  h: MyObjectStore<"jmdict_h", HStores>
  senses: MyObjectStore<"jmdict_senses", SenseStores>
  entry: Jmdict & { mdate: string }
}) {
  ok(!("_id" in entry))
  if ("_id" in entry) {
    throw new Error("_id should not be in entry")
  }

  // Refresh index entries for the headwords
  const hs = await h.index("e").getAll(entry.ent_seq)
  for (const item of hs) {
    await h.delete(item.id)
  }
  await createHeadwordEntries({ h, entry })

  // Refresh sense entries
  await createSenseEntries({ senses, entry })

  // Save entry
  const { sense: _, ...rest } = entry
  await jmdict.put(rest)
}

export async function* getGlossResultsForQueryOffline(
  q: string,
  signal: AbortSignal
) {}

/**
 * Headword index document
 */
export interface JmdictH {
  /**
   * `${h}!${e}`
   */
  id: string
  /**
   * The ent_seq identifier of the entry.
   */
  e: number
  /**
   * The indexed headword.
   */
  h: string
}
