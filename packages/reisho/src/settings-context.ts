import { createContext } from "react"

export const SettingsContext = createContext<Settings>({
  posLanguage: "key",
  setPosLanguage: () => undefined,
})

export interface Settings {
  posLanguage: "key" | "en" | "ja"
  setPosLanguage(value: Settings["posLanguage"]): void
}
