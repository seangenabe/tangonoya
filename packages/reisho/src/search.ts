import { Jmdict } from "@tangonoya/provider-jmdict/types.gen"
import lunr from "lunr"

export async function buildIndexes(jmdictEntries: Iterable<Jmdict>) {
  return lunr(function () {
    this.ref("ent_seq")
    this.field("en")

    for (const entry of jmdictEntries) {
      for (const sense of entry.sense) {
        for (const gloss of sense.gloss) {
          if (gloss.lang === "en" || gloss.lang == null) {
            this.add({
              ent_seq: entry.ent_seq,
              en: gloss.value,
            })
          }
        }
      }
    }
  })
}
