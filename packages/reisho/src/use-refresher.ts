import { useState } from "react"

export function useRefresher() {
  const [token, setToken] = useState({})
  const refresh = () => setToken(() => ({}))

  return { token, refresh }
}
