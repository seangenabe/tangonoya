import { createContext } from "react"

export const LanguagesContext = createContext<ReadonlyMap<string, string>>(
  new Map()
)

export interface LanguageCodeEntry {
  code: string
  description: string
}
