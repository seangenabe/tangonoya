import { createContext } from "react"
import { isBrowser } from "./is-browser"

export function createDictionaryDownloaderContext<
  T extends DictionaryDownloaderContextObject
>(defaults: T) {
  return createContext<T>(defaults)
}
export interface DictionaryDownloaderContextObject {
  downloadingAc: AbortController | "aborting" | null
  mdate: string | null
  downloadStartTime: Date
  download(): void
  cancelDownload(): void
  clearData(): void
}

export interface JmdictDictionaryDownloaderContextObject
  extends DictionaryDownloaderContextObject {
  downloadedCount: number
  totalCount: number
}

export const JmdictDictionaryDownloaderContext =
  createDictionaryDownloaderContext<JmdictDictionaryDownloaderContextObject>({
    downloadingAc: isBrowser() ? new AbortController() : null,
    mdate: null,
    downloadedCount: 0,
    totalCount: 0,
    downloadStartTime: new Date(),
    download: () => undefined,
    cancelDownload: () => undefined,
    clearData: () => undefined,
  })

export interface WanikaniDictionaryDownloaderContextObject
  extends DictionaryDownloaderContextObject {
  downloadedCount: number
  totalCount: number
}

export const WanikaniDictionaryDownloaderContext =
  createDictionaryDownloaderContext<WanikaniDictionaryDownloaderContextObject>({
    downloadingAc: isBrowser() ? new AbortController() : null,
    mdate: null,
    downloadedCount: 0,
    totalCount: 0,
    downloadStartTime: new Date(),
    download: () => undefined,
    cancelDownload: () => undefined,
    clearData: () => undefined,
  })
