import {
  Entry,
  Kanji,
  Reading,
  SubjectType,
  Vocabulary,
} from "@tangonoya/provider-wanikani/types.gen"
import readerAsIterator from "fetch-ndjson"
import { from } from "ix/asynciterable"
import { buffer, map } from "ix/asynciterable/operators"
import ky from "ky"
import { openDictionaryDb } from "./dictionary"
import { WanikaniDictionaryDownloaderContextObject } from "./dictionary-downloader-context"
import { keyRangeForSearchMode } from "./key-range-for-search-mode"

const REPORT_INTERVAL = 20
const BUFFER_ITEM_COUNT = 1000

export async function* getWanikaniEntries({
  q,
  signal,
  searchMode,
  dictionaryDownloader,
}: {
  q: string
  signal: AbortSignal
  searchMode: "exact" | "prefix"
  dictionaryDownloader: WanikaniDictionaryDownloaderContextObject
}) {
  if (navigator.onLine) {
    // Online
    yield* getEntriesForQueryOnline(q, searchMode, signal)
    return
  }
  if (!dictionaryDownloader.mdate) {
    return
  }
  // Offline
  yield* getEntriesForQueryOffline(q, searchMode)
}

export async function downloadDictionaryData({
  signal,
  progress = () => undefined,
}: {
  signal?: AbortSignal
  progress?: (i: number) => void
}) {
  const { mdate }: { mdate: string } = await ky
    .get("dict/wanikani/meta.json")
    .json()
  const response = await ky.get("dict/wanikani/entries.ndjson", {
    signal,
    headers: {
      accept: "application/x-ndjson",
    },
  })

  const reader = response.body.getReader()
  const iterator = readerAsIterator(reader) as AsyncGenerator<Entry>
  const db = await openDictionaryDb()

  let i = 0

  const iter = from(iterator).pipe(
    buffer(BUFFER_ITEM_COUNT),
    map((entries) => {
      const t = db.transaction(["wanikani", "wanikani_h"], "readwrite")
      const wanikani = t.objectStore("wanikani")
      const h = t.objectStore("wanikani_h")

      const promises: Promise<unknown>[] = []

      for (const entry of entries) {
        const { _id, ...rest } = entry as Entry & { _id: number }

        if (i % REPORT_INTERVAL === 0) {
          progress(i)
        }
        i++

        promises.push(wanikani.put(rest))

        if (isKanji(rest) || isVocab(rest)) {
          promises.push(
            h.put({
              id: `${rest.data.characters}-${_id}`,
              e: _id,
              h: rest.data.characters,
            }),
            ...(rest.data.readings as Reading[]).map((r) =>
              h.put({
                id: `${r.reading}-${_id}`,
                e: _id,
                h: r.reading,
              })
            )
          )
        }
      }

      return Promise.all(promises)
    })
  )

  for await (const _ of iter) {
  }

  await db.put("meta", { mdate }, "wanikani")
}

export function isKanji(entry: Entry): entry is Entry & { data: Kanji } {
  return entry.object === SubjectType.kanji
}

export function isVocab(entry: Entry): entry is Entry & { data: Vocabulary } {
  return entry.object === SubjectType.vocabulary
}

export async function* getEntriesForQueryOnline(
  q: string,
  searchMode: "exact" | "prefix",
  signal?: AbortSignal
): AsyncGenerator<Entry> {
  if (q.trim() === "") {
    return
  }
  const response = await ky.get("wanikani/search", {
    prefixUrl: process.env.NEXT_PUBLIC_BACKEND_URL,
    searchParams: {
      q,
      search_mode: searchMode,
    },
    signal,
    headers: {
      accept: "application/x-ndjson",
    },
  })

  const reader = response.body.getReader()
  const iterator = readerAsIterator(reader)

  yield* iterator
}

export async function* getEntriesForQueryOffline(
  q: string,
  searchMode: "exact" | "prefix"
): AsyncGenerator<Entry> {
  if (q.trim() === "") {
    return
  }

  const db = await openDictionaryDb()
  const h = db.transaction("wanikani_h", "readonly")
  const seen = new Set<number>()

  const indexItems: WanikaniH[] = await h.store
    .index("h")
    .getAll(keyRangeForSearchMode(q, searchMode))

  for (const { e } of indexItems) {
    if (seen.has(e)) {
      continue
    }

    const entry = await db.transaction("wanikani").store.get(e)
    yield entry

    seen.add(e)
  }
}

export interface WanikaniH {
  id: string
  e: number
  h: string
}
