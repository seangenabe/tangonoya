import { from } from "ix/asynciterable"
import { buffer } from "ix/asynciterable/operators"
import { useCallback, useEffect, useRef, useState } from "react"
import { useRefresher } from "./use-refresher"

export function useIterableLoader<T = unknown>(
  iterable: AsyncIterable<T>,
  {
    onError = () => undefined,
    onAbort,
    pageSize = 20,
    disabled,
  }: {
    onError?(err: unknown): void
    onAbort?(err: unknown): void
    pageSize?: number
    disabled?: boolean
  }
): AsyncIterableLoader<T> {
  const [items, setItems] = useState<readonly T[]>([])
  const [done, setDone] = useState(false)
  const iteratorRef = useRef<AsyncIterator<readonly T[]>>()
  const locksRef = useRef(new Set())
  const { token, refresh } = useRefresher()

  useEffect(() => {
    const bufferedIterable = from(iterable).pipe(buffer(pageSize))
    iteratorRef.current = bufferedIterable[Symbol.asyncIterator]()
    refresh()
  }, [iterable, pageSize])

  const loadNext = useCallback(
    async (reset?: boolean) => {
      if (!reset && (locksRef.current.size > 0 || done)) {
        return
      }
      const myLock = {}

      try {
        const newItems: T[] = []
        let newDone = false

        locksRef.current.add(myLock)

        const iterationResult = await iteratorRef.current.next()
        if (iterationResult.done) {
          newDone = true
        } else {
          newItems.push(...iterationResult.value)
          if (newItems.length < pageSize) {
            newDone = true
          }
        }

        setDone(newDone)

        setItems((items) => {
          if (reset) {
            return newItems
          }
          return items.concat(newItems)
        })
      } catch (err) {
        if (onAbort && err.name === "AbortError") {
          onAbort(err)
        } else {
          onError(err)
        }
      } finally {
        locksRef.current.delete(myLock)
      }
    },
    [token, done, locksRef.current, onAbort, onError]
  )

  useEffect(() => {
    locksRef.current = new Set()

    // Run once.
    loadNext(true)
  }, [iteratorRef.current])

  return {
    items,
    done,
    loadNext,
    locked: locksRef.current.size > 0,
  }
}

export interface AsyncIterableLoader<T> {
  items: readonly T[]
  done: boolean
  loadNext(): Promise<void>
  locked: boolean
}
