const withPwa = require("next-pwa")
const withTm = require("next-transpile-modules")

// https://developers.google.com/web/tools/workbox/reference-docs/latest/module-workbox-build#.RuntimeCachingEntry
// Default runtime cache:
//   https://github.com/shadowwalker/next-pwa/blob/master/cache.js
const runtimeCaching = [
  {
    urlPattern: "/",
    handler: "NetworkFirst",
    options: {
      // don't change cache name
      cacheName: "start-url",
      expiration: {
        maxEntries: 1,
        purgeOnQuotaError: false,
      },
    },
  },
  {
    urlPattern: /\.(?:eot|otf|ttc|ttf|woff|woff2|font.css)$/i,
    handler: "StaleWhileRevalidate",
    options: {
      cacheName: "static-font-assets",
      expiration: {
        maxEntries: 4,
      },
    },
  },
  {
    urlPattern: /\.(?:jpg|jpeg|gif|png|svg|ico|webp)$/i,
    handler: "StaleWhileRevalidate",
    options: {
      cacheName: "static-image-assets",
      expiration: {
        maxEntries: 64,
      },
    },
  },
  {
    urlPattern: /\.(?:js)$/i,
    handler: "StaleWhileRevalidate",
    options: {
      cacheName: "static-js-assets",
      expiration: {
        maxEntries: 32,
      },
    },
  },
  {
    urlPattern: /\.(?:css|less)$/i,
    handler: "StaleWhileRevalidate",
    options: {
      cacheName: "static-style-assets",
      expiration: {
        maxEntries: 32,
      },
    },
  },
  {
    urlPattern: /\.(?:json|xml|csv)$/i,
    handler: "NetworkFirst",
    options: {
      cacheName: "static-data-assets",
      expiration: {
        maxEntries: 32,
      },
    },
  },
  {
    urlPattern: "/settings",
    handler: "NetworkFirst",
    options: {
      cacheName: "others",
      expiration: {
        maxEntries: 1,
        purgeOnQuotaError: false,
      },
    },
  },
]

module.exports = withTm(["ky"])(
  withPwa({
    pwa: {
      // Next.js 9+
      dest: "public",
      runtimeCaching,
    },
  })
)
