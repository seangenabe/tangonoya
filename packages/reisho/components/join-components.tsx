import React, {
  ComponentType,
  createElement,
  Fragment,
  ReactNode,
  ReactText,
} from "react"

export function JoinComponents({
  separator = null,
  children,
}: {
  separator?: ComponentType | string | number
  children: ReactNode
}) {
  if (Array.isArray(children)) {
    return (
      <>
        {children.flatMap((child, index) =>
          index === 0
            ? [child]
            : [separator && createNode(separator, index), child]
        )}
      </>
    )
  }
  return <>{children}</>
}

function createNode(
  n: ComponentType | ReactText | null | undefined,
  key: ReactText
) {
  if (n == null) {
    return n
  }
  if (typeof n === "string" || typeof n === "number") {
    return n
  }
  return <Fragment key={key}>{createElement(n)}</Fragment>
}
