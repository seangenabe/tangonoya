import React, { ComponentType, ReactNode } from "react"

export function Spaced({
  as,
  children,
}: {
  as: ComponentType
  children: ReactNode
}) {
  return React.createElement(as, {}, children)
}
