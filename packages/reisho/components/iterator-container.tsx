import { Box, BoxProps, Button, Spinner } from "@chakra-ui/react"
import React, {
  ReactNode,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from "react"
import { useInView } from "react-intersection-observer"
import { AsyncIterableLoader } from "../src/use-iterable-loader"

export function IterableContainer<TInput>({
  loader,
  children,
  containerProps = {},
  heading = null,
}: IteratorContainerProps<TInput>) {
  const { ref, inView } = useInView()
  const [loadNextMode, setLoadNextMode] =
    useState<"intersection" | "button">("button")

  useEffect(() => {
    if (inView && !loader.locked) {
      loader.loadNext()
    }
  }, [inView, loader.locked, loadNextMode])

  const clickHandler = useCallback(() => {
    loader.loadNext()
  }, [])

  const loadNextElement = useMemo(() => {
    if (loader.done) {
      return null
    }

    if (loadNextMode === "button") {
      return (
        <Box textAlign="center" pb="4">
          <Button onClick={clickHandler}>Load Next</Button>
        </Box>
      )
    } else if (loadNextMode === "intersection") {
      return loader.done ? null : (
        <Box textAlign="center" ref={ref} pb="4">
          <Spinner padding="4" />
        </Box>
      )
    }
  }, [loadNextMode, loader.done])

  if (loader.done && loader.items.length === 0) {
    return null
  }

  return (
    <Box {...containerProps}>
      {heading()}
      {loader.items.map((item, index) => children(item, index))}
      {loadNextElement}
    </Box>
  )
}

export interface IteratorContainerProps<TInput> {
  loader: AsyncIterableLoader<TInput>
  onError?: (error: any) => void
  children: (item: TInput, index: number) => ReactNode
  containerProps?: BoxProps
  heading?: () => ReactNode
}
