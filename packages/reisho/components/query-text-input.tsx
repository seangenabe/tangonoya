import { SearchIcon } from "@chakra-ui/icons"
import {
  Input,
  InputGroup,
  InputLeftElement,
  VisuallyHidden,
} from "@chakra-ui/react"
import { debounce } from "lodash"
import React, { useCallback, useEffect, useState } from "react"
import { useQuery } from "../src/use-query"

export function QueryTextInput() {
  const [q, setQ] = useQuery()
  const [textInput, setTextInput] = useState(q)

  useEffect(() => setTextInput(q), [q])

  const inputChanged = useCallback(
    debounce((q: string) => setQ(q), 300),
    [setQ]
  )

  const updateTextInput = (value: string) => {
    setTextInput(value)
    inputChanged(value)
  }

  return (
    <InputGroup>
      <InputLeftElement pointerEvents="none">
        <SearchIcon />
      </InputLeftElement>
      <VisuallyHidden>Search: </VisuallyHidden>
      <Input
        placeholder="English or Japanese text"
        size="md"
        value={textInput}
        onChange={(e) => updateTextInput(e.target.value)}
      />
    </InputGroup>
  )
}
