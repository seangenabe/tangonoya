import { HamburgerIcon, MoonIcon, SunIcon } from "@chakra-ui/icons"
import {
  IconButton,
  Menu,
  MenuButton,
  MenuDivider,
  MenuItem,
  MenuItemOption,
  MenuList,
  MenuOptionGroup,
  useColorMode,
} from "@chakra-ui/react"
import { useRouter } from "next/router"
import React, { useContext } from "react"
import { SettingsContext } from "../src/settings-context"

export function SettingsMenu() {
  const { colorMode, toggleColorMode } = useColorMode()
  const { posLanguage, setPosLanguage } = useContext(SettingsContext)
  const router = useRouter()

  return (
    <Menu>
      <MenuButton
        as={IconButton}
        aria-label="Settings"
        icon={<HamburgerIcon />}
        size="md"
        variant="outline"
      />
      <MenuList>
        <MenuItem
          icon={colorMode === "light" ? <MoonIcon /> : <SunIcon />}
          onClick={() => toggleColorMode()}
        >
          Toggle theme
        </MenuItem>
        <MenuOptionGroup
          title="Part-of-speech language"
          type="radio"
          value={posLanguage}
          onChange={(value: typeof posLanguage) => setPosLanguage(value)}
        >
          <MenuItemOption value="key">Key</MenuItemOption>
          <MenuItemOption value="en">English</MenuItemOption>
          <MenuItemOption value="ja">日本語</MenuItemOption>
        </MenuOptionGroup>
        <MenuDivider />
        <MenuItem onClick={() => router.push("/settings")}>
          More settings
        </MenuItem>
      </MenuList>
    </Menu>
  )
}
