import { Box, BoxProps, useColorModeValue } from "@chakra-ui/react"
import React from "react"

export function Pill(props: PillProps) {
  const bg = useColorModeValue("gray.200", "gray.700")
  const color = useColorModeValue("gray.900", "white")

  return (
    <Box
      borderRadius="base"
      paddingX="2"
      paddingY="1"
      as="span"
      bg={bg}
      color={color}
      {...props}
    />
  )
}

export type PillProps = BoxProps
