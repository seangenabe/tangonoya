import {
  Button,
  ButtonProps,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  useDisclosure,
} from "@chakra-ui/react"
import React, { useCallback } from "react"

export function DeleteDataConfirmationModal({
  onConfirm,
  buttonProps,
}: {
  onConfirm: () => void
  buttonProps?: ButtonProps
}) {
  const { isOpen, onOpen, onClose } = useDisclosure()

  const onDeleteConfirm = useCallback(() => {
    onConfirm()
    onClose()
  }, [])

  return (
    <>
      <Button onClick={onOpen} {...buttonProps}>
        Clear data
      </Button>

      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Confirm delete data</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            Are you sure you want to delete the dictionary data?
          </ModalBody>

          <ModalFooter>
            <Button colorScheme="blue" mr={3} onClick={onClose}>
              Close
            </Button>
            <Button
              variant="solid"
              onClick={() => onDeleteConfirm()}
              colorScheme="red"
            >
              Delete
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  )
}
