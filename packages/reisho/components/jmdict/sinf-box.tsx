import React from "react"
import { Pill } from "../pill"

export function SinfBox({ sInf }: { sInf: string }) {
  return <Pill opacity="0.5">{sInf}</Pill>
}
