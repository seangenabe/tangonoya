import { WarningIcon } from "@chakra-ui/icons"
import { Box, Flex, Stack, Tooltip } from "@chakra-ui/react"
import {
  Kanji,
  KeInfValues,
  KeRePriValues,
  Reading,
} from "@tangonoya/provider-jmdict/types.gen"
import React, { ComponentPropsWithoutRef, ReactNode } from "react"
import { BsFillStarFill, BsStarHalf } from "react-icons/bs"
import { Pill } from "../pill"
import { AtejiBox } from "./ateji-box"
import { InfBox } from "./inf-box"
import { PriBox } from "./pri-box"

const COMMON_PRI = new Set([
  "news1",
  "ichi1",
  "spec1",
  "spec2",
  "gai1",
]) as ReadonlySet<KeRePriValues>

export function HeadwordBox(
  props: {
    node: ReactNode
    kanji?: Kanji
    reading?: Reading
  } & ComponentPropsWithoutRef<typeof Pill>
) {
  const { node, kanji, reading } = props

  const infs = [...(kanji?.ke_inf ?? []), ...(reading?.re_inf ?? [])]
  const showSideIcons = reading?.re_pri || infs.length !== 0
  const infsNonAteji = infs.filter((inf) => inf !== KeInfValues.ateji)

  return (
    <Pill
      backgroundColor="purple.500"
      color="white"
      whiteSpace="nowrap"
      paddingLeft="1rem"
      paddingRight={showSideIcons ? 0 : "1rem"}
      display="flex"
      {...props}
    >
      <Box display="-webkit-box" lang="ja">
        {node}
      </Box>
      {showSideIcons ? (
        <Flex
          width="1.5rem"
          fontSize="xs"
          flexDirection="column"
          alignItems="flex-end"
          padding="0.25rem"
        >
          {reading?.re_pri ? (
            <Tooltip
              placement="right-start"
              shouldWrapChildren
              label={
                <Stack direction="row">
                  {reading.re_pri.map((pri) => (
                    <PriBox pri={pri} key={pri} />
                  ))}
                </Stack>
              }
            >
              {reading.re_pri.some((pri) => COMMON_PRI.has(pri)) ? (
                <BsFillStarFill />
              ) : (
                <BsStarHalf />
              )}
            </Tooltip>
          ) : null}
          {infs.includes(KeInfValues.ateji) ? <AtejiBox /> : null}

          {infsNonAteji.length !== 0 && (
            <Tooltip
              placement="right-start"
              shouldWrapChildren
              label={
                <Stack direction="row">
                  {infsNonAteji.map((inf) => (
                    <InfBox key={inf} inf={inf} />
                  ))}
                </Stack>
              }
            >
              <WarningIcon />
            </Tooltip>
          )}
        </Flex>
      ) : null}
    </Pill>
  )
}
