import {
  Box,
  HeadingProps,
  Tag,
  useColorModeValue,
  VisuallyHidden,
} from "@chakra-ui/react"
import {
  Jmdict,
  Kanji,
  MiscValues,
  Reading,
  Sense,
} from "@tangonoya/provider-jmdict/types.gen"
import React, { ReactNode } from "react"
import { CommonEntryBox } from "../common/entry-box"
import { CommonEntryBoxHeading } from "../common/entry-box-heading"
import { CreateReadings } from "../create-readings"
import { JoinComponents } from "../join-components"
import { HeadwordBox } from "./headword-box"
import { SenseBox } from "./sense-box"

export function EntryBox({ entry }: { entry: Jmdict }) {
  const headwords: { node: ReactNode; kanji?: Kanji; reading?: Reading }[] = []
  const seenReadings = new Set<string>()

  if (entry.k_ele) {
    headwords.push(
      ...entry.k_ele
        .flatMap((kanji) =>
          entry.r_ele.map((reading) => {
            // If reading is restricted to a single kanji
            if (reading.re_restr && !reading.re_restr.includes(kanji.keb)) {
              return null
            }
            // If reading is not to be associated with a kanji element
            if (reading.re_nokanji) {
              return null
            }

            return {
              node: <CreateReadings kanji={kanji.keb} reading={reading.reb} />,
              kanji,
              reading,
            }
          })
        )
        .filter(Boolean)
    )
    const readingsWithoutKanji = entry.r_ele.filter(
      (reading) => reading.re_nokanji
    )

    // Usually kana
    if (
      entry.sense.every((sense) => (sense.misc ?? []).includes(MiscValues.uk))
    ) {
      // Prioritize re_nokanji readings
      headwords.unshift(
        ...readingsWithoutKanji.map((reading) => ({
          node: reading.reb,
          reading,
        }))
      )
      for (const reading of readingsWithoutKanji) {
        seenReadings.add(reading.reb)
      }
    } else {
      headwords.push(
        ...readingsWithoutKanji.map((reading) => ({
          node: reading.reb,
          reading,
        }))
      )
    }
  } else {
    headwords.push(
      ...entry.r_ele.map((reading) => ({ node: reading.reb, reading }))
    )
  }

  // Check if all senses are tagged `uk`.
  if (
    entry.sense.every((sense) =>
      (sense.misc ?? []).some(
        (x) => x === MiscValues.arch || x === MiscValues.uk
      )
    )
  ) {
    // Add a kana-only reading if not present.
    headwords.unshift(
      ...(function* () {
        for (const reading of entry.r_ele) {
          if (seenReadings.has(reading.reb)) {
            continue
          }
          if (!headwords.some((h) => h.node === reading.reb)) {
            yield { node: reading.reb, reading }
          }
        }
      })()
    )
  }

  return (
    <CommonEntryBox>
      <EntryBoxHeading headwords={headwords} p="4" pb="0" />
      <EntryBoxBody entry={entry} />
    </CommonEntryBox>
  )
}

function EntryBoxHeading(
  props: {
    headwords: { node: ReactNode; kanji?: Kanji }[]
  } & HeadingProps
) {
  const { headwords, ...headingProps } = props

  return (
    <CommonEntryBoxHeading {...headingProps}>
      <JoinComponents separator={() => <VisuallyHidden>,</VisuallyHidden>}>
        {headwords.slice(0, 1).map((h) => (
          <HeadwordBox key="0" {...h} />
        ))}
        {headwords.slice(1).map((h, index) => (
          <HeadwordBox
            key={index + 1}
            fontSize="xl"
            layerStyle="faded"
            {...h}
          />
        ))}
      </JoinComponents>
    </CommonEntryBoxHeading>
  )
}

function EntryBoxBody({ entry }: { entry: Jmdict }) {
  return (
    <Box display="grid" gridTemplateColumns="auto 1fr" pb="4">
      {entry.sense.map((sense, index) => (
        <SenseRow key={index} sense={sense} index={index} />
      ))}
    </Box>
  )
}

function SenseRow({ sense, index }: { sense: Sense; index: number }) {
  const accentColor = useColorModeValue("#e8e8e8", "#171717")
  return (
    <>
      <Box
        bg={index % 2 === 1 ? accentColor : null}
        py="0.5em"
        pl="4"
        display="flex"
        alignItems="flex-start"
        justifyContent="flex-end"
      >
        <Tag variant="subtle">{index + 1}.</Tag>
      </Box>
      <Box
        key={index}
        bg={index % 2 === 1 ? accentColor : null}
        py="0.5em"
        pl="0.5em"
        pr="4"
        display="flex"
      >
        <SenseBox sense={sense} flex="1" />
      </Box>
    </>
  )
}
