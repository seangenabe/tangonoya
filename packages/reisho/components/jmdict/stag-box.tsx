import { Tooltip } from "@chakra-ui/react"
import React from "react"
import { Pill } from "../pill"

export function StagkBox({ stagk }: { stagk: string }) {
  return (
    <Tooltip
      placement="right-start"
      shouldWrapChildren
      label={`Limited to the headword ${stagk}`}
    >
      <Pill>{stagk}</Pill>
    </Tooltip>
  )
}

export function StagrBox({ stagr }: { stagr: string }) {
  return (
    <Tooltip
      placement="right-start"
      shouldWrapChildren
      label={`Limited to the reading ${stagr}`}
    >
      <Pill>{stagr}</Pill>
    </Tooltip>
  )
}
