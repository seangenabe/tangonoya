import { Text, Tooltip } from "@chakra-ui/react"
import { Lsource } from "@tangonoya/provider-jmdict/types.gen"
import React, { useContext } from "react"
import { LanguagesContext } from "../../src/languages-context"
import { Pill } from "../pill"

export function LsourceBox({ lsource }: { lsource: Lsource }) {
  const languagesMap = useContext(LanguagesContext)
  const language = languagesMap.get(lsource.lang) ?? lsource.lang ?? "English"

  return (
    <Pill opacity="0.5">
      {`From ${language} `}
      <i>{lsource.value}</i>
      {lsource.ls_type === "part" ? " (partial)" : ""}
      {lsource.ls_wasei ? (
        <>
          {" ("}
          <Tooltip label="Wasei-eigo, etc.">Pseudo-loanword</Tooltip>)
        </>
      ) : (
        ""
      )}
    </Pill>
  )
}
