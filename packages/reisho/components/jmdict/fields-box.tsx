import entities from "@tangonoya/provider-jmdict/entities.json"
import { FieldValues } from "@tangonoya/provider-jmdict/types.gen"
import React from "react"
import { Pill } from "../pill"

const FIELDS_MAP = new Map(
  entities.field.map((field) => [field.name, field.value])
)

export function FieldsBox({ fields }: { fields: FieldValues[] }) {
  return (
    <Pill as="em">{`(${fields.map((field) => fieldDescription(field))})`}</Pill>
  )
}

export function fieldDescription(field: FieldValues) {
  const description = FIELDS_MAP.get(field) ?? field

  return description
}
