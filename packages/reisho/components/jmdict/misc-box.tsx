import React from "react"
import entities from "@tangonoya/provider-jmdict/entities.json"
import { MiscValues } from "@tangonoya/provider-jmdict/types.gen"
import { Pill } from "../pill"
import { Tooltip } from "@chakra-ui/react"

const MISC_MAP = new Map(entities.misc.map(({ name, value }) => [name, value]))

export function MiscBox({ misc }: { misc: MiscValues }) {
  return (
    <Tooltip
      label={MISC_MAP.get(misc) ?? misc}
      placement="right-start"
      shouldWrapChildren
    >
      <Pill opacity="0.5">{misc}</Pill>
    </Tooltip>
  )
}
