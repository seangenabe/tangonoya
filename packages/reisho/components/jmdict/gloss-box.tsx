import { Text, VisuallyHidden } from "@chakra-ui/react"
import { Gloss, GTypeValues } from "@tangonoya/provider-jmdict/types.gen"
import React from "react"
import { Pill, PillProps } from "../pill"

export function GlossBox({
  gloss,
  pillProps,
}: {
  gloss: Gloss
  pillProps?: PillProps
}) {
  return (
    <Pill {...pillProps} as={gloss.g_type === "expl" ? "i" : null}>
      <GTypePrefix gType={gloss.g_type} />
      {gloss.value}
    </Pill>
  )
}

function GTypePrefix({ gType }: { gType: GTypeValues }) {
  switch (gType) {
    case "expl":
      return <VisuallyHidden>(explanation) </VisuallyHidden>
    case "fig":
      return (
        <>
          <Text as="span" opacity="0.5">
            (<i>figurative</i>)
          </Text>{" "}
        </>
      )
    case "lit":
      return (
        <>
          <Text as="span" opacity="0.5">
            (<i>literal</i>)
          </Text>{" "}
        </>
      )
  }
  return null
}
