import { Tooltip, useColorModeValue } from "@chakra-ui/react"
import entities from "@tangonoya/provider-jmdict/entities.json"
import { PosValues } from "@tangonoya/provider-jmdict/types.gen"
import React, { useContext } from "react"
import { SettingsContext } from "../../src/settings-context"
import { Pill } from "../pill"

const SUBSTITUTIONS_EN: [
  string | RegExp,
  string | ((substring: string, ...args: any[]) => string)
][] = [
  [" (keiyoushi)", ""],
  ["yoi/ii", "よい/いい"],
  ["'kari'", "'かり'"],
  ["'kari'", "'く'"],
  ["'no'", "'の'"],
  ["'shiku'", "'しく'"],
  ["'taru'", "'たる'"],
  ["'to'", "'と'"],
  ["'u'", "'う'"],
  ["'bu'", "'ぶ'"],
  ["'dzu'", "'づ'"],
  ["'gu'", "'ぐ'"],
  ["'hu/fu'", "'ふ'"],
  ["'ku'", "'く'"],
  ["'mu'", "'む'"],
  [/\bnu\b/g, "ぬ"],
  [/\bru\b/g, "る"],
  [/\b-ri\b/g, "〜り"],
  [/\bsu\b/g, "す"],
  ["'tsu'", "'つ'"],
  ["'we'", "'ゑ'"],
  ["'yu'", "'ゆ'"],
  ["'zu'", "'ず'"],
  ["-aru", "-ある"],
  [/^[INYG].*dan/g, (s) => s.toLowerCase()],
  ["Iku/Yuku", "いく/ゆく"],
  [/\bUru\b/g, "うる"],
  [/\bEru\b/g, "える"],
  [/\bKuru\b/g, "くる"],
  [/\bsuru\b/g, "する"],
  [/\bzuru\b/g, "ずる"],
  [/\b-jiru\b/g, "〜じる"],
]

const LOCALIZED = [
  { key: "adj-i", ja: "形容詞" },
  { key: "adj-na", en: "adjectival noun", ja: "形容動詞" },
  { key: "adj-pn", ja: "連体詞" },
  { key: "adv", en: "adverb", ja: "副詞" },
  { key: "int", ja: "感動詞" },
  { key: "n", en: "common noun", ja: "普通名詞" },
  { key: "n-adv", ja: "副詞的名詞" },
  { key: "n-t", ja: "時相名詞" },
  { key: "v1", ja: "一段動詞" },
]
const LOCALIZED_MAP = new Map(LOCALIZED.map((x) => [x.key, x]))

const POS_MAP: ReadonlyMap<
  string,
  { key: string; en: string; ja?: string }
> = new Map(
  entities.pos.map((pos) => [
    pos.name,
    {
      key: pos.name,
      en: substituteEn(pos.value),
      ...(LOCALIZED_MAP.get(pos.name) ?? {}),
    },
  ])
)

function substituteEn(value: string) {
  let ret = value
  for (const [regex, replacement] of SUBSTITUTIONS_EN) {
    ret = ret.replaceAll(regex, replacement as string)
  }
  return ret
}

const posTheme = {
  noun: {
    backgroundColor: "blue.500",
    color: "white",
  },
  verb: {
    backgroundColor: "red.700",
    color: "white",
  },
  adjective: {
    backgroundColor: "yellow.700",
    color: "white",
  },
  adverb: {
    backgroundColor: "orange.700",
    color: "white",
  },
  pronoun: {
    backgroundColor: "blue.700",
    color: "white",
  },
}

export function PosBox({ pos }: { pos: PosValues }) {
  const theme = (() => {
    if (pos === "n-adv") {
      return {
        ...posTheme.noun,
        bgGradient: "linear(to-r, blue.700, orange.700)",
      }
    }
    if (/^n($|-)/.test(pos)) {
      return posTheme.noun
    }
    if (pos === "pn") {
      return posTheme.pronoun
    }
    if (pos.startsWith("v")) {
      return posTheme.verb
    }
    if (pos.startsWith("adj-")) {
      if (pos === "adj-na") {
        return {
          ...posTheme.adjective,
          bgGradient: "linear(to-r, yellow.700, blue.500)",
        }
      }
      return posTheme.adjective
    }
    if (pos.startsWith("adv")) {
      return posTheme.adverb
    }
    return { bg: useColorModeValue("gray.100", "gray.800") }
  })()

  const { posLanguage } = useContext(SettingsContext)
  let content: string
  const posObj = POS_MAP.get(pos)
  if (posObj) {
    if (posLanguage === "ja") {
      content = posObj.ja ?? posObj.en ?? pos
    } else if (posLanguage === "en") {
      content = posObj.en ?? pos
    } else {
      content = pos
    }
  } else {
    content = pos
  }

  return (
    <Tooltip
      label={posLanguage === "en" ? null : posObj.en}
      placement="right-start"
      shouldWrapChildren
    >
      <Pill whiteSpace="nowrap" cursor="default" {...theme}>
        {content}
      </Pill>
    </Tooltip>
  )
}
