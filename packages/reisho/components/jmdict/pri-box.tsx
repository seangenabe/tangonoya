import { KeRePriValues } from "@tangonoya/provider-jmdict/types.gen"
import React from "react"
import { Pill } from "../pill"

export function PriBox({ pri }: { pri: KeRePriValues }) {
  return <Pill>{pri}</Pill>
}
