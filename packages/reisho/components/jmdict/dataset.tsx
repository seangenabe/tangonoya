import {
  ReactNode,
  useMemo,
  createContext,
  useContext,
  useCallback,
} from "react"
import { Dataset } from "./datasets/dataset-interface"
import {
  JapaneseDataset,
  useJapaneseDataset,
} from "./datasets/japanese-dataset"

const JmdictDatasetManagerContext = createContext<DatasetManager | undefined>(
  undefined
)
JmdictDatasetManagerContext.displayName = "JmdictDatasetManager"

export const JmdictDatasetRawProvider = JmdictDatasetManagerContext.Provider

const languages = ["dut", "eng"]

export function JmdictDatasetManager({ children }: { children: ReactNode }) {
  const japaneseDataset = useJapaneseDataset()

  const clearAll = useCallback(async () => {
    await Promise.all([japaneseDataset.clearDataset()])
  }, [])

  const value = useMemo<DatasetManager>(
    () => ({
      japaneseDataset,
      // glossDatasets,
      clearAll,
    }),
    []
  )

  return (
    <JmdictDatasetRawProvider value={value}>
      {children}
    </JmdictDatasetRawProvider>
  )
}

export function useJmdictDatasetManager() {
  const value = useContext(JmdictDatasetManagerContext)
  return value
}

export interface DatasetManager {
  japaneseDataset: JapaneseDataset
  // glossDatasets: ReadonlyMap<string, GlossDataset>
  clearAll(): Promise<void>
}

export interface GlossDataset extends Dataset {
  findGlossMatch(q: string, signal: AbortSignal): Promise<void>
}
