import React from "react"
import entities from "@tangonoya/provider-jmdict/entities.json"
import { DialValues } from "@tangonoya/provider-jmdict/types.gen"
import { Pill } from "../pill"

const DIALECT_MAP = new Map(
  entities.dial.map(({ name, value }) => [name, value])
)

export function DialBox({ dial }: { dial: DialValues }) {
  return <Pill opacity="0.5">({DIALECT_MAP.get(dial) ?? dial})</Pill>
}
