import React from "react"
import { Link, useColorModeValue } from "@chakra-ui/react"

export function XrefLink({ xref }: { xref: string }) {
  const bb = useColorModeValue("gray.700", "gray.300")
  const hoverBg = useColorModeValue(
    "rgba(0,0,0, 0.25)",
    "rgba(255,255,255,0.25)"
  )

  return (
    <Link
      lang="ja"
      borderBottom="1px"
      borderBottomStyle="dashed"
      borderBottomColor={bb}
      _hover={{
        borderBottomStyle: "solid",
        bg: hoverBg,
      }}
      href="#"
    >
      {xref}
    </Link>
  )
}
