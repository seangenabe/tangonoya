import entities from "@tangonoya/provider-jmdict/entities.json"
import { KeInfValues, ReInfValues } from "@tangonoya/provider-jmdict/types.gen"
import React from "react"
import { Pill } from "../pill"

const INFS_MAP = new Map(
  [...entities.ke_inf, ...entities.re_inf].map((inf) => [inf.name, inf.value])
)

export function InfBox({ inf }: { inf: KeInfValues | ReInfValues }) {
  return <Pill key={inf}>{INFS_MAP.get(inf) ?? inf}</Pill>
}
