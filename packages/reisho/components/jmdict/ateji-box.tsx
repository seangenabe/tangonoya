import { Tooltip } from "@chakra-ui/react"
import React from "react"
import { BsVolumeUpFill } from "react-icons/bs"

export function AtejiBox() {
  return (
    <Tooltip
      label="ateji"
      aria-label="ateji"
      shouldWrapChildren
      placement="right-start"
    >
      <BsVolumeUpFill />
    </Tooltip>
  )
}
