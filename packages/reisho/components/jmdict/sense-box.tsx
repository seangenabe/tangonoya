import { Flex, FlexProps, VisuallyHidden } from "@chakra-ui/react"
import { Sense } from "@tangonoya/provider-jmdict/types.gen"
import React from "react"
import { JoinComponents } from "../join-components"
import { Pill } from "../pill"
import { DialBox } from "./dial-box"
import { FieldsBox } from "./fields-box"
import { GlossBox } from "./gloss-box"
import { LsourceBox } from "./lsource-box"
import { MiscBox } from "./misc-box"
import { PosBox } from "./pos-box"
import { SinfBox } from "./sinf-box"
import { StagkBox, StagrBox } from "./stag-box"
import { XrefLink } from "./xref-link"

const ENSP = " "

export function SenseBox(props: { sense: Sense } & FlexProps) {
  const { sense } = props
  return (
    <Flex
      sx={{ gap: ["0.5em", "1.5em"] }}
      alignItems="baseline"
      flexDirection={["column", "row"]}
    >
      <SenseQualifiersBox sense={sense} />
      <SenseMainBox sense={sense} />
    </Flex>
  )
}

export function SenseQualifiersBox({ sense }: { sense: Sense }) {
  return (
    <Flex flexDirection="column" sx={{ gap: "1em" }}>
      <Flex alignItems="baseline" layerStyle="spaced">
        <VisuallyHidden>(</VisuallyHidden>
        <JoinComponents separator={() => <VisuallyHidden>,</VisuallyHidden>}>
          {(sense.pos ?? []).map((pos) => (
            <PosBox pos={pos} key={pos} />
          ))}
        </JoinComponents>
        <VisuallyHidden>{`)${ENSP}`}</VisuallyHidden>
      </Flex>
      {(sense.stagk || sense.stagr) && (
        <Flex alignItems="baseline" layerStyle="spaced">
          {sense.stagk?.map((stagk) => (
            <StagkBox key={stagk} stagk={stagk} />
          ))}
          {sense.stagr?.map((stagr) => (
            <StagrBox key={stagr} stagr={stagr} />
          ))}
        </Flex>
      )}
      {sense.misc && (
        <Flex alignItems="baseline" layerStyle="spaced">
          {sense.misc.map((misc) => (
            <MiscBox misc={misc} key={misc} />
          ))}
        </Flex>
      )}
    </Flex>
  )
}

export function SenseMainBox({ sense }: { sense: Sense }) {
  return (
    <Flex flex="1" flexDirection="column" layerStyle="spaced">
      <Flex
        flexDirection={["column", "row"]}
        flexWrap={["initial", "wrap"]}
        layerStyle="spaced"
        alignItems="baseline"
      >
        {sense.field ? <FieldsBox fields={sense.field} /> : null}
        <JoinComponents
          separator={() => <VisuallyHidden>{`;${ENSP}`}</VisuallyHidden>}
        >
          {(sense.gloss ?? []).map((gloss) => (
            <GlossBox
              gloss={gloss}
              pillProps={{ flexShrink: 1 }}
              key={gloss.value}
            />
          ))}
        </JoinComponents>
      </Flex>
      {(sense.xref || sense.lsource || sense.dial || sense.s_inf) && (
        <Flex layerStyle="spaced" flexWrap="wrap">
          {sense.xref && (
            <Pill opacity="0.5">
              See also:{" "}
              <JoinComponents separator="、">
                {sense.xref?.map((xref) => (
                  <XrefLink key={xref} xref={xref} />
                ))}
              </JoinComponents>
            </Pill>
          )}
          {sense.lsource?.map((lsource) => (
            <LsourceBox
              key={`${lsource.lang}-${lsource.value}`}
              lsource={lsource}
            />
          ))}
          {sense.dial?.map((dial) => (
            <DialBox key={dial} dial={dial} />
          ))}
          {sense.s_inf?.map((sInf) => (
            <SinfBox key={sInf} sInf={sInf} />
          ))}
        </Flex>
      )}
    </Flex>
  )
}
