import { useToast } from "@chakra-ui/react"
import { JmdictMetaJson } from "@tangonoya/provider-jmdict"
import { Jmdict } from "@tangonoya/provider-jmdict/types.gen"
import { ok } from "assert"
import readerAsIterator from "fetch-ndjson"
import { IDBPObjectStore, StoreNames } from "idb"
import { deleteDB } from "idb/with-async-ittr-cjs"
import { from } from "ix/asynciterable"
import { buffer, map } from "ix/asynciterable/operators"
import ky from "ky"
import { useCallback, useEffect, useMemo, useState } from "react"
import {
  DbSchema,
  JmdictIndexedSense,
  JmdictReduced,
  openDictionaryDb,
} from "src/dictionary"
import { isBrowser } from "src/is-browser"
import { keyRangeForSearchMode } from "src/key-range-for-search-mode"
import { readAll } from "src/read-all"
import { Dataset } from "./dataset-interface"

async function deleteDictionaryDb() {
  await deleteDB("dictionary")
}

const REPORT_INTERVAL = 100
const BUFFER_ITEM_COUNT = 1000

export function useJapaneseDataset(): JapaneseDataset {
  const toast = useToast()
  const [mdate, setMdate] = useState<string | null>(null)
  const [status, setStatus] = useState<JapaneseDataset["status"]>("no-offline")

  // Get current status
  useEffect(() => {
    ;(async () => {
      try {
        const db = await openDictionaryDb()
        const jmdictMeta = (await db.get("meta", "jmdict")) as
          | { mdate: string }
          | undefined
        setMdate(jmdictMeta?.mdate ?? "")
        setStatus(jmdictMeta ? "downloaded" : "no-offline")
      } catch (err) {
        console.error(err)
        toast({
          title: "Error occurred.",
          description:
            "An error occurred while initializing your client database.",
          status: "error",
          isClosable: true,
        })
      }
    })()
  }, [])

  const clearDataset = useCallback(async () => {
    try {
      ok(status === "downloaded")

      await deleteDictionaryDb()
      setMdate(null)
    } catch (err) {
      console.error(err)
      toast({
        title: "Error occurred.",
        description:
          "An error occurred while deleting the dictionary database.",
        status: "error",
        isClosable: true,
      })
    }
  }, [status])

  const downloadDataset = useCallback<JapaneseDataset["downloadDataset"]>(
    async ({
      signal = new AbortController().signal,
      progress = () => undefined,
    }) => {
      ok(!isBrowser)
      ok(status === "no-offline")

      const { totalCount } = (await ky
        .get("jmdict/stats", { prefixUrl: process.env.NEXT_PUBLIC_BACKEND_URL })
        .json()) as { totalCount: number }

      const downloadStartTime = new Date()

      const { mdate }: JmdictMetaJson = await ky
        .get("/dict/jmdict/meta.json")
        .json()

      const entriesResponse = await ky.get("/dict/jmdict/entries.ndjson", {
        signal,
        headers: {
          accept: "application/x-ndjson",
        },
      })

      ok(entriesResponse.body)

      const entriesReader = entriesResponse.body.getReader()
      const entriesIterator = readerAsIterator(
        entriesReader
      ) as AsyncGenerator<Jmdict>
      const db = await openDictionaryDb()

      let downloadedCount = 0

      await readAll(
        from(entriesIterator).pipe(
          buffer(BUFFER_ITEM_COUNT),
          map(async (entries): Promise<void> => {
            const t = db.transaction(["jmdict", "jmdict_h"], "readwrite")
            const jmdict = t.objectStore("jmdict")
            const h = t.objectStore("jmdict_h")

            for (const entry of entries) {
              const { _id, ...rest } = entry as Jmdict & { _id: number }

              await addNewJmdictEntry({ jmdict, h, entry: { ...entry, mdate } })
              downloadedCount++

              if (downloadedCount % REPORT_INTERVAL === 0) {
                progress({ totalCount, downloadedCount })
              }
            }
          })
        )
      )
    },
    [status]
  )

  const findJapaneseExactMatch = useCallback<
    JapaneseDataset["findJapaneseExactMatch"]
  >(async function* findJapaneseExactMatch(q: string, signal: AbortSignal) {
    if (q.trim() === "") {
      return
    }

    if (navigator.onLine) {
      // Online
      const response = await ky.get("jmdict/search", {
        prefixUrl: process.env.NEXT_PUBLIC_BACKEND_URL,
        searchParams: {
          q,
          search_mode: "exact",
        },
        signal,
        headers: {
          accept: "application/x-ndjson",
        },
      })

      ok(response.body)
      const reader = response.body.getReader()
      const iterator = readerAsIterator(reader)

      yield* iterator
    }

    // Offline

    if (!mdate) {
      return
    }

    const db = await openDictionaryDb()
    const h = db.transaction("jmdict_h", "readonly")
    const seen = new Set<number>()

    let indexItems: JmdictH[] = []
    indexItems = await h.store
      .index("h")
      .getAll(keyRangeForSearchMode(q, "exact"))

    for (const { e } of indexItems) {
      if (seen.has(e)) {
        continue
      }

      const entry = await db.transaction("jmdict").store.get(e)
      ok(entry)
      const indexedSenses: JmdictIndexedSense[] = await db
        .transaction("jmdict_senses")
        .store.index("ent_seq")
        .getAll(e)

      yield { ...entry, sense: indexedSenses.map((s) => s.sense) }

      seen.add(e)
    }
  }, [])

  const findJapanesePrefixMatch = useCallback<
    JapaneseDataset["findJapanesePrefixMatch"]
  >(async function* findJapanesePrefixMatch(q: string, signal: AbortSignal) {
    if (q.trim() === "") {
      return
    }

    if (navigator.onLine) {
      // Online
      const response = await ky.get("jmdict/search", {
        prefixUrl: process.env.NEXT_PUBLIC_BACKEND_URL,
        searchParams: {
          q,
          search_mode: "exact",
        },
        signal,
        headers: {
          accept: "application/x-ndjson",
        },
      })

      ok(response.body)
      const reader = response.body.getReader()
      const iterator = readerAsIterator(reader)

      yield* iterator
    }

    // Offline

    if (!mdate) {
      return
    }

    const db = await openDictionaryDb()
    const h = db.transaction("jmdict_h", "readonly")
    const seen = new Set<number>()

    let indexItems: JmdictH[] = []
    indexItems = await h.store
      .index("h")
      .getAll(keyRangeForSearchMode(q, "exact"))

    for (const { e } of indexItems) {
      if (seen.has(e)) {
        continue
      }

      const entry = await db.transaction("jmdict").store.get(e)
      ok(entry)
      const indexedSenses: JmdictIndexedSense[] = await db
        .transaction("jmdict_senses")
        .store.index("ent_seq")
        .getAll(e)

      yield { ...entry, sense: indexedSenses.map((s) => s.sense) }

      seen.add(e)
    }
  }, [])

  const ret = useMemo<JapaneseDataset>(
    () => ({
      mdate,
      clearDataset,
      downloadDataset,
      status,
      findJapaneseExactMatch,
      findJapanesePrefixMatch,
    }),
    [mdate, clearDataset]
  )

  return ret
}

async function addNewJmdictEntry<
  JmdictStores extends StoreNames<DbSchema>[] = "jmdict"[],
  HStores extends StoreNames<DbSchema>[] = "jmdict_h"[],
  SenseStores extends StoreNames<DbSchema>[] = "jmdict_senses"[]
>({
  jmdict,
  h,
  senses,
  entry,
}: {
  jmdict: MyObjectStore<"jmdict", JmdictStores>
  h: MyObjectStore<"jmdict_h", HStores>
  senses?: MyObjectStore<"jmdict_senses", SenseStores>
  entry: Jmdict & { sense?: Jmdict["sense"]; mdate: string }
}) {
  ok(!("_id" in entry))

  // Create entry
  const { sense: _, ...rest } = entry
  await jmdict.put(rest)

  await createHeadwordEntries({ h, entry })

  // Create sense entries
  if (senses && entry.sense) {
    await createSenseEntries({ senses, entry })
  }
}

type MyObjectStore<
  T extends StoreNames<DbSchema>,
  Stores extends StoreNames<DbSchema>[] = StoreNames<DbSchema>[]
> = IDBPObjectStore<DbSchema, Stores, T, "readwrite">

export interface JapaneseDataset extends Dataset {
  mdate: string | null
  findJapaneseExactMatch(
    q: string,
    signal: AbortSignal
  ): AsyncIterable<JmdictReduced & { mdate: string }>
  findJapanesePrefixMatch(
    q: string,
    signal: AbortSignal
  ): AsyncIterable<JmdictReduced & { mdate: string }>
}

/**
 * Create entries for headwords
 */
async function createHeadwordEntries<
  HStores extends StoreNames<DbSchema>[] = "jmdict_h"[]
>({
  h,
  entry,
}: {
  h: MyObjectStore<"jmdict_h", HStores>
  entry: Jmdict & { sense?: Jmdict["sense"]; mdate: string }
}) {
  for (const k_ele of entry.k_ele ?? []) {
    await h.put({
      id: `${k_ele.keb}!${entry.ent_seq}`,
      e: entry.ent_seq,
      h: k_ele.keb,
    })
  }

  for (const r_ele of entry.r_ele ?? []) {
    await h.put({
      id: `${r_ele.reb}!${entry.ent_seq}`,
      e: entry.ent_seq,
      h: r_ele.reb,
    })
  }
}

/**
 * Create sense entries
 */
async function createSenseEntries<
  SenseStores extends StoreNames<DbSchema>[] = "jmdict_senses"[]
>({
  senses,
  entry,
}: {
  senses: MyObjectStore<"jmdict_senses", SenseStores>
  entry: Pick<Jmdict, "ent_seq" | "sense">
}) {
  // TODO: Filter unused languages
  for (const [senseIndex, senseValue] of entry.sense.entries()) {
    await senses.put({
      id: `${entry.ent_seq}.${senseIndex}`,
      ent_seq: entry.ent_seq,
      sense: senseValue,
    })
  }
}

async function updateJmdictEntry<
  JmdictStores extends StoreNames<DbSchema>[] = "jmdict"[],
  HStores extends StoreNames<DbSchema>[] = "jmdict_h"[],
  SenseStores extends StoreNames<DbSchema>[] = "jmdict_senses"[]
>({
  jmdict,
  h,
  senses,
  entry,
}: {
  jmdict: MyObjectStore<"jmdict", JmdictStores>
  h: MyObjectStore<"jmdict_h", HStores>
  senses: MyObjectStore<"jmdict_senses", SenseStores>
  entry: Jmdict & { mdate: string }
}) {
  ok(!("_id" in entry))
  if ("_id" in entry) {
    throw new Error("_id should not be in entry")
  }

  // Refresh index entries for the headwords
  const hs = await h.index("e").getAll(entry.ent_seq)
  for (const item of hs) {
    await h.delete(item.id)
  }
  await createHeadwordEntries({ h, entry })

  // Refresh sense entries
  await createSenseEntries({ senses, entry })

  // Save entry
  const { sense: _, ...rest } = entry
  await jmdict.put(rest)
}

export async function* getGlossResultsForQueryOffline(
  q: string,
  signal: AbortSignal
) {}

/**
 * Headword index document
 */
export interface JmdictH {
  /**
   * `${h}!${e}`
   */
  id: string
  /**
   * The ent_seq identifier of the entry.
   */
  e: number
  /**
   * The indexed headword.
   */
  h: string
}
