export interface Dataset {
  status: "no-offline" | "downloading" | "downloaded"
  downloadDataset(opts: {
    signal?: AbortSignal
    progress?(progressInfo: {
      totalCount: number
      downloadedCount: number
    }): void
  }): Promise<void>
  clearDataset(): Promise<void>
}
