import React from "react"
import Head from "next/head"

export function Title({ title }: { title?: string }) {
  const displayTitle = title ? `Reisho | ${title}` : "Reisho"

  return (
    <Head>
      <title>{displayTitle}</title>
      <meta property="og:title" content={displayTitle} key="og:title" />
      <meta name="twitter:title" content={displayTitle} />
    </Head>
  )
}
