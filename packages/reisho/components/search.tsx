import { Box, Container, Flex, useColorModeValue } from "@chakra-ui/react"
import React, { useState } from "react"
import { DictionaryResults } from "../components/dictionary-results"
import { QueryTextInput } from "../components/query-text-input"
import { useQuery } from "../src/use-query"
import { SettingsMenu } from "./settings-menu"
import { Settings as Settings, SettingsContext } from "../src/settings-context"
import { Title } from "./title"

export function Search() {
  const [q] = useQuery()
  const bg = useColorModeValue("gray.50", "gray.800")
  const [posLanguage, setPosLanguage] = useState<Settings["posLanguage"]>("key")

  return (
    <SettingsContext.Provider value={{ posLanguage, setPosLanguage }}>
      <Title title={q} />
      <Box as="main" bgColor={bg}>
        <Container maxWidth="80rem">
          <Flex
            layerStyle="spaced"
            paddingY={[0, 4]}
            position="sticky"
            top="0"
            bg={bg}
            zIndex="900"
          >
            <QueryTextInput />
            <SettingsMenu />
          </Flex>
          <DictionaryResults />
        </Container>
      </Box>
    </SettingsContext.Provider>
  )
}
