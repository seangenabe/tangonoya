import { Meaning } from "@tangonoya/provider-wanikani/types.gen"
import React from "react"
import { Pill, PillProps } from "../pill"

export function GlossBox({
  meaning,
  pillProps,
}: {
  meaning: Meaning
  pillProps?: PillProps
}) {
  return (
    <Pill opacity={meaning.primary ? null : 0.5} {...pillProps}>
      {meaning.meaning}
    </Pill>
  )
}
