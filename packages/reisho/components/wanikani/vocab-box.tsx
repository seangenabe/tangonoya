import {
  Box,
  BoxProps,
  Flex,
  HeadingProps,
  VisuallyHidden,
} from "@chakra-ui/react"
import {
  Entry,
  Reading,
  SubjectType,
  Vocabulary,
} from "@tangonoya/provider-wanikani/types.gen"
import React from "react"
import { CommonEntryBox } from "../common/entry-box"
import { CommonEntryBoxHeading } from "../common/entry-box-heading"
import { CreateReadings } from "../create-readings"
import { JoinComponents } from "../join-components"
import { GlossBox } from "./gloss-box"
import { HeadwordBox } from "./headword-box"

export function VocabBox({ entry }: { entry: Vocab }) {
  const { data } = entry

  return (
    <CommonEntryBox>
      <WanikaniVocabBoxHeading
        kanji={data.characters}
        readings={data.readings}
        p="4"
        pb="0"
      />
      <WanikaniVocabBoxBody entry={entry} p="4" pt="0" />
    </CommonEntryBox>
  )
}

function WanikaniVocabBoxHeading(
  props: {
    kanji: string
    readings: Reading[]
  } & HeadingProps
) {
  const { kanji, readings, ...headingProps } = props
  return (
    <CommonEntryBoxHeading {...headingProps}>
      <JoinComponents separator={() => <VisuallyHidden>,</VisuallyHidden>}>
        {readings.map((reading) => (
          <HeadwordBox key={reading.reading}>
            <CreateReadings kanji={kanji} reading={reading.reading} />
          </HeadwordBox>
        ))}
      </JoinComponents>
    </CommonEntryBoxHeading>
  )
}

function WanikaniVocabBoxBody({
  entry,
  ...boxProps
}: { entry: Vocab } & BoxProps) {
  return (
    <Box gridTemplateColumns="auto 1fr" {...boxProps}>
      <Flex flexWrap="wrap" layerStyle="spaced" alignItems="baseline">
        <JoinComponents
          separator={() => <VisuallyHidden>{", "}</VisuallyHidden>}
        >
          {entry.data.meanings.map((meaning) => (
            <GlossBox
              meaning={meaning}
              pillProps={{ flexShrink: 1 }}
              key={meaning.meaning}
            />
          ))}
        </JoinComponents>
      </Flex>
    </Box>
  )
}

type Vocab = Entry & { object: SubjectType.vocabulary; data: Vocabulary }
