import { Box } from "@chakra-ui/react"
import React, { ComponentPropsWithoutRef } from "react"
import { Pill } from "../pill"

export function HeadwordBox(props: ComponentPropsWithoutRef<typeof Pill>) {
  const { children, ...pillProps } = props
  return (
    <Pill
      backgroundColor="purple.500"
      color="white"
      whiteSpace="nowrap"
      paddingLeft="1rem"
      paddingRight="1rem"
      display="flex"
      {...pillProps}
    >
      <Box display="-webkit-box" lang="ja">
        {children}
      </Box>
    </Pill>
  )
}
