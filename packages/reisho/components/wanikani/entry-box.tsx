import {
  Entry,
  SubjectType,
  Vocabulary,
} from "@tangonoya/provider-wanikani/types.gen"
import React from "react"
import { VocabBox } from "./vocab-box"

export function WanikaniEntryBox({ entry }: { entry: Entry }) {
  if (isVocab(entry)) {
    return <VocabBox entry={entry} />
  }
  return null
}

function isVocab(
  entry: Entry
): entry is Entry & { object: SubjectType.vocabulary; data: Vocabulary } {
  return entry.object === SubjectType.vocabulary
}
