import { useToast } from "@chakra-ui/react"
import ky from "ky"
import React, { ReactNode, useCallback, useEffect, useState } from "react"
import { deleteDictionaryDb, openDictionaryDb } from "../../src/dictionary"
import { downloadDictionaryData } from "../../src/wanikani"
import { WanikaniDictionaryDownloaderContext } from "../../src/dictionary-downloader-context"
import { isBrowser } from "../../src/is-browser"

export function WanikaniDictionaryDownloader({
  children,
}: {
  children: ReactNode
}) {
  const [downloadingAc, setDownloadingAc] = useState<
    AbortController | null | "aborting"
  >(null)
  const toast = useToast()
  const [mdate, setMdate] = useState<string | null>(null)
  const [downloadedCount, setDownloadedCount] = useState(0)
  const [totalCount, setTotalCount] = useState(0)
  const [downloadStartTime, setDownloadStartTime] = useState(new Date())

  useEffect(() => {
    ;(async () => {
      try {
        const db = await openDictionaryDb()
        const wanikaniMeta = await db.get("meta", "wanikani")
        setMdate(wanikaniMeta?.mdate ?? "")
      } catch (err) {
        console.error(err)
        toast({
          title: "Error occurred.",
          description:
            "An error occurred while initializing your client database.",
          status: "error",
          isClosable: true,
        })
      }
    })()
  }, [])

  const download = useCallback(async () => {
    if (!isBrowser()) {
      return
    }

    const ac = new AbortController()

    setDownloadingAc(ac)

    try {
      setDownloadedCount(0)
      const { totalCount } = (await ky
        .get("wanikani/stats", {
          prefixUrl: process.env.NEXT_PUBLIC_BACKEND_URL,
        })
        .json()) as { totalCount: number }
      setTotalCount(totalCount)
      setDownloadStartTime(new Date())

      await downloadDictionaryData({
        signal: ac.signal,
        progress: (i) => setDownloadedCount(i),
      })
      toast({
        title: "Data downloaded.",
        status: "success",
        isClosable: true,
      })

      const db = await openDictionaryDb()
      const wanikaniMeta = await db.get("meta", "wanikani")
      setMdate(wanikaniMeta?.mdate ?? "")
    } catch (err) {
      if (err.name === "AbortError") {
        toast({
          title: "Download cancelled.",
          description: "The download has been cancelled.",
          status: "warning",
          isClosable: true,
        })
        return
      }
      console.error(err)
      toast({
        title: "Error occurred.",
        description:
          "An error occurred while downloading dictionary data. Please try again.",
        status: "error",
        isClosable: true,
      })
    } finally {
      setDownloadingAc(null)
    }
  }, [])

  const cancelDownload = useCallback(() => {
    if (downloadingAc && typeof downloadingAc !== "string") {
      downloadingAc.abort()
      setDownloadingAc("aborting")
    }
  }, [downloadingAc])

  const clearData = useCallback(async () => {
    try {
      await deleteDictionaryDb()
      setMdate("")
    } catch (err) {
      console.error(err)
      toast({
        title: "Error occurred.",
        description:
          "An error occurred while deleting the dictionary database.",
        status: "error",
        isClosable: true,
      })
    }
  }, [])

  return (
    <WanikaniDictionaryDownloaderContext.Provider
      value={{
        downloadingAc,
        mdate,
        downloadedCount,
        totalCount,
        downloadStartTime,
        download,
        cancelDownload,
        clearData,
      }}
    >
      {children}
    </WanikaniDictionaryDownloaderContext.Provider>
  )
}
