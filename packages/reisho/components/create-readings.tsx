import React, { ReactNode, memo } from "react"
import { generateRuby } from "../src/generate-ruby"

export const Ruby = memo(function Ruby({
  children,
  reading,
}: {
  children: string
  reading: string
}) {
  if (!children) {
    return null
  }
  if (children === reading) {
    return <>{children}</>
  }
  return (
    <ruby>
      {children}
      <rp>(</rp>
      <rt>{reading}</rt>
      <rp>)</rp>
    </ruby>
  )
})

export const CreateReadings = memo(function CreateReadings({
  kanji,
  reading,
}: {
  kanji: string
  reading: string
}) {
  const rubyObjects = generateRuby(kanji, reading)

  return (
    <>
      {rubyObjects.map((r, i) => {
        if (typeof r === "string") {
          return r
        }
        return (
          <Ruby key={i} reading={r.reading}>
            {r.body}
          </Ruby>
        )
      })}
    </>
  )
})
