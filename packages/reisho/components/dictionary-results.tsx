import {
  Alert,
  AlertIcon,
  Box,
  Code,
  Heading,
  Stack,
  useToast,
} from "@chakra-ui/react"
import { Jmdict } from "@tangonoya/provider-jmdict/types.gen"
import { Entry } from "@tangonoya/provider-wanikani/types.gen"
import { concat, empty, from } from "ix/asynciterable"
import { filter, map } from "ix/asynciterable/operators"
import React, {
  ComponentPropsWithoutRef,
  ReactNode,
  useCallback,
  useContext,
  useEffect,
  useRef,
  useState,
} from "react"
import {
  JmdictDictionaryDownloaderContext,
  WanikaniDictionaryDownloaderContext,
} from "../src/dictionary-downloader-context"
import { getJmdictEntries } from "../src/jmdict"
import { useIterableLoader } from "../src/use-iterable-loader"
import { useQuery } from "../src/use-query"
import { getWanikaniEntries, isVocab } from "../src/wanikani"
import { IterableContainer } from "./iterator-container"
import { EntryBox as JmdictEntryBox } from "./jmdict/entry-box"
import { WanikaniEntryBox } from "./wanikani/entry-box"

// @refresh reset
export function DictionaryResults(props: ComponentPropsWithoutRef<typeof Box>) {
  const [q] = useQuery()
  const abortControllerRef = useRef<AbortController | undefined>(undefined)
  const jmdictDictionaryDownloader = useContext(
    JmdictDictionaryDownloaderContext
  )
  const wanikaniDictionaryDownloader = useContext(
    WanikaniDictionaryDownloaderContext
  )
  const toast = useToast()
  const errorHandler = useCallback((err) => {
    if (err.name === "AbortError") {
      console.info(err)
      return
    }
    console.error(err)
    toast({
      title: "Error occurred.",
      description: "An error occurred while retrieving dictionary entries.",
      status: "error",
      isClosable: true,
    })
  }, [])
  const [exactIterable, setExactIterable] = useState<
    AsyncIterable<DictionaryEntry>
  >(empty())
  const [prefixIterable, setPrefixIterable] = useState<
    AsyncIterable<DictionaryEntry>
  >(empty())
  const [glossIterable, setGlossIterable] = useState<
    AsyncIterable<DictionaryEntry>
  >(empty())
  const exactLoader = useIterableLoader(exactIterable, {
    onError: errorHandler,
  })
  const prefixLoader = useIterableLoader(prefixIterable, {
    onError: errorHandler,
  })
  const glossLoader = useIterableLoader(glossIterable, {
    onError: errorHandler,
  })

  useEffect(() => {
    abortControllerRef.current = new AbortController()
    const [jmdictExact, jmdictPrefix] = (["exact", "prefix"] as const).map(
      (searchMode) =>
        getJmdictEntries({
          q,
          signal: abortControllerRef.current!.signal,
          searchMode,
          dictionaryDownloader: jmdictDictionaryDownloader,
        })
    )

    const [wanikaniExact, wanikaniPrefix] = (["exact", "prefix"] as const).map(
      (searchMode) =>
        getWanikaniEntries({
          q,
          signal: abortControllerRef.current!.signal,
          searchMode,
          dictionaryDownloader: wanikaniDictionaryDownloader,
        })
    )

    const jmdictGloss = getJmdictEntries({
      q,
      signal: abortControllerRef.current.signal,
      searchMode: "gloss",
      dictionaryDownloader: jmdictDictionaryDownloader,
    })

    const wanikaniVocabExact = from(wanikaniExact).pipe(filter(isVocab))
    const wanikaniVocabPrefix = from(wanikaniPrefix).pipe(filter(isVocab))

    setExactIterable(
      concat(
        from(wanikaniVocabExact).pipe(
          map((entry) => ({ type: "wanikani" as const, entry }))
        ),
        from(jmdictExact).pipe(
          map((entry) => ({ type: "jmdict" as const, entry }))
        )
      )
    )

    setPrefixIterable(
      concat(
        from(wanikaniVocabPrefix).pipe(
          map((entry) => ({ type: "wanikani" as const, entry }))
        ),
        from(jmdictPrefix).pipe(
          map((entry) => ({ type: "jmdict" as const, entry }))
        )
      )
    )

    setGlossIterable(
      from(jmdictGloss).pipe(
        map((entry) => ({ type: "jmdict" as const, entry }))
      )
    )

    return () => {
      abortControllerRef.current!.abort()
    }
  }, [q, jmdictDictionaryDownloader, wanikaniDictionaryDownloader])

  let content: ReactNode

  if (q.trim() === "") {
    content = (
      <Alert status="info">
        <AlertIcon />
        Start typing Japanese or gloss text on the field above to search for
        dictionary entries.
      </Alert>
    )
  } else if (
    [exactLoader, prefixLoader, glossLoader].every(
      (loader) => loader.done && loader.items.length === 0
    )
  ) {
    content = (
      <Stack spacing={3}>
        <Alert status="info">
          <AlertIcon />
          <span>
            No results found for query <Code>{q}</Code>
          </span>
        </Alert>
        {!navigator.onLine && (
          <Alert status="warning">
            <AlertIcon />
            You are offline. We won't be able to show you results from sources
            that you haven't downloaded for offline use.
          </Alert>
        )}
      </Stack>
    )
  } else {
    content = (
      <>
        <Heading as="h2" pt="2" pb="8">
          Dictionary Results
        </Heading>
        <section>
          <IterableContainer
            onError={errorHandler}
            loader={exactLoader}
            heading={() => <SectionHeading>Exact matches</SectionHeading>}
          >
            {(x) => {
              if (x.type === "jmdict") {
                return <JmdictEntryBox entry={x.entry} key={x.entry.ent_seq} />
              } else if (x.type === "wanikani") {
                return <WanikaniEntryBox entry={x.entry} key={x.entry.id} />
              }
            }}
          </IterableContainer>
        </section>
        <section>
          <IterableContainer
            onError={errorHandler}
            loader={prefixLoader}
            heading={() => <SectionHeading>Prefix matches</SectionHeading>}
          >
            {(x) => {
              if (x.type === "jmdict") {
                return <JmdictEntryBox entry={x.entry} key={x.entry.ent_seq} />
              } else if (x.type === "wanikani") {
                return <WanikaniEntryBox entry={x.entry} key={x.entry.id} />
              }
            }}
          </IterableContainer>
        </section>
        <section>
          <IterableContainer
            onError={errorHandler}
            loader={glossLoader}
            heading={() => <SectionHeading>Gloss matches</SectionHeading>}
          >
            {(x) => {
              if (x.type === "jmdict") {
                return <JmdictEntryBox entry={x.entry} key={x.entry.ent_seq} />
              }
            }}
          </IterableContainer>
        </section>
      </>
    )
  }

  return <Box {...props}>{content}</Box>
}

function SectionHeading({ children }: { children: ReactNode }) {
  return (
    <Heading as="h3" py="2" fontWeight="normal" fontSize="3xl">
      {children}
    </Heading>
  )
}

type DictionaryEntry =
  | { type: "jmdict"; entry: Jmdict }
  | { type: "wanikani"; entry: Entry }
