import { CSSObject, Flex, useColorModeValue } from "@chakra-ui/react"
import React, { ReactNode, useMemo } from "react"

export function CommonEntryBox({
  children,
  optimize,
}: {
  children: ReactNode
  optimize?: boolean
}) {
  const bg = useColorModeValue("white", "black")
  const sx = useMemo(() => {
    const ret: CSSObject = { gap: "1em", containIntrinsicSize: "500px" }
    if (optimize) {
      ret.contentVisibility = "auto"
    }
    return ret
  }, [optimize])

  return (
    <Flex
      as="article"
      boxShadow="base"
      marginBottom="4"
      flexDirection="column"
      borderRadius="base"
      sx={sx}
      bg={bg}
    >
      {children}
    </Flex>
  )
}
