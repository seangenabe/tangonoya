import { Heading, HeadingProps } from "@chakra-ui/react"
import React, { ReactNode } from "react"

export function CommonEntryBoxHeading(
  props: { children: ReactNode } & HeadingProps
) {
  const { children } = props
  return (
    <Heading
      as="h3"
      size="lg"
      display="flex"
      flexWrap="wrap"
      alignItems="baseline"
      layerStyle="spaced"
      {...props}
    >
      {children}
    </Heading>
  )
}
