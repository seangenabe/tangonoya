const { copyFile, mkdir, readFile, readdir } = require("fs/promises")
const { paths } = require("@tangonoya/common")

async function copyDict() {
  const pMap = (await import("p-map")).default

  const dictDirectory = `${__dirname}/public/dict`
  await mkdir(`${dictDirectory}/jmdict`, { recursive: true })
  await mkdir(`${dictDirectory}/wanikani`, { recursive: true })

  // Copy Jmdict files
  const jmdictFiles = await readdir(`${paths.data}/jmdict`)
  await Promise.all(
    jmdictFiles.map(
      async (file) =>
        await copyFile(
          `${paths.data}/jmdict/${file}`,
          `${__dirname}/public/dict/jmdict/${file}`
        )
    )
  )

  const { languagesUsed } = await JSON.parse(
    await readFile(`${paths.data}/jmdict/meta.json`, { encoding: "utf8" })
  )
  console.log("Copying...")

  await pMap(
    languagesUsed,
    async (lang) => {
      await copyFile(
        `${paths.data}/jmdict/entries-${lang}.ndjson`,
        `${__dirname}/public/dict/jmdict/entries-${lang}.ndjson`
      )
    },
    { concurrency: 4 }
  )

  console.log("Copy JMdict files done.")

  // Copy Wanikani files
  const wanikaniFiles = await readdir(`${paths.data}/wanikani`)
  await Promise.all(
    wanikaniFiles.map(
      async (file) =>
        await copyFile(
          `${paths.data}/wanikani/${file}`,
          `${__dirname}/public/dict/wanikani/${file}`
        )
    )
  )
  console.log("Copy Wanikani files done.")
}

if (require.main === module) {
  ;(async () => {
    try {
      await copyDict()
    } catch (err) {
      console.error(err)
      process.exit(1)
    }
  })()
}
