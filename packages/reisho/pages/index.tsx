import { Search } from "../components/search"
import { LanguageCodeEntry, LanguagesContext } from "../src/languages-context"

export default function Home({
  languages,
}: {
  languages: LanguageCodeEntry[]
}) {
  const languagesMap = new Map(
    languages.map(({ code, description }) => [code, description])
  )
  return (
    <LanguagesContext.Provider value={languagesMap}>
      <Search />
    </LanguagesContext.Provider>
  )
}

export async function getStaticProps() {
  const got = (await import("got")).default

  let languages: LanguageCodeEntry[] = [
    {
      code: "eng",
      description: "English",
    },
  ]

  do {
    if (process.env.CI) {
      break
    }
    try {
      languages = await got
        .get("jmdict/languages", {
          prefixUrl: process.env.NEXT_PUBLIC_BACKEND_URL,
        })
        .json()
    } catch (err) {}
  } while (false)

  return {
    props: {
      languages,
    },
  }
}
