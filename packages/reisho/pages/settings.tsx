import { ArrowBackIcon, DownloadIcon } from "@chakra-ui/icons"
import {
  Box,
  Button,
  Container,
  Flex,
  IconButton,
  Spinner,
  Table,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tr,
  useColorModeValue,
} from "@chakra-ui/react"
import { useRouter } from "next/router"
import React, { ReactNode, useContext } from "react"
import { DeleteDataConfirmationModal } from "../components/delete-data-confirmation-modal"
import { Title } from "../components/title"
import {
  DictionaryDownloaderContextObject,
  JmdictDictionaryDownloaderContext,
  WanikaniDictionaryDownloaderContext,
} from "../src/dictionary-downloader-context"

export default function SettingsPage() {
  const bg = useColorModeValue("gray.50", "gray.800")
  const router = useRouter()
  const downloaders = [
    {
      key: "jmdict",
      name: "JMdict",
      downloader: useContext(JmdictDictionaryDownloaderContext),
    },
    {
      key: "wanikani",
      name: "WaniKani",
      downloader: useContext(WanikaniDictionaryDownloaderContext),
    },
  ]

  return (
    <Box>
      <Title title="Settings" />
      <Box as="main" bgColor={bg}>
        <Container maxWidth="80rem">
          <Flex
            layerStyle="spaced"
            paddingY={[0, 4]}
            position="sticky"
            top="0"
            bg={bg}
            zIndex="900"
            alignItems="baseline"
          >
            <IconButton
              aria-label="Back"
              icon={<ArrowBackIcon />}
              onClick={() => router.push("/")}
            />
            <Text fontSize="xl">Reisho</Text>
          </Flex>
          <Text as="h2" fontSize="2xl" py="4">
            Manage offline data
          </Text>
          <Table variant="striped" colorScheme="blue">
            <Thead>
              <Tr>
                <Th>Source</Th>
                <Th>Status</Th>
                <Th>Actions</Th>
              </Tr>
            </Thead>
            <Tbody>
              {downloaders.map((o) => (
                <DownloaderRow {...o} downloaderKey={o.key} key={o.key} />
              ))}
            </Tbody>
          </Table>
        </Container>
      </Box>
    </Box>
  )
}

export function DownloaderRow({
  downloaderKey,
  name,
  downloader,
}: {
  downloaderKey: string
  name: string
  downloader: DictionaryDownloaderContextObject
}) {
  const {
    mdate,
    downloadingAc,
    cancelDownload,
    downloadStartTime,
    download,
    clearData,
  } = downloader
  let status: ReactNode

  const controls: ReactNode[] = []

  // check if meta has been initialized
  // initialized if mdate is a string
  if (mdate == null) {
    status = <Spinner size="sm" />
  } else {
    // check if data has been downloaded
    // downloaded if mdate is not empty
    if (mdate === "") {
      if (downloadingAc === "aborting") {
        controls.push(
          <Button key="abortDownload" disabled isLoading>
            Cancelling download
          </Button>
        )
        status = "Aborting download"
      } else if (downloadingAc) {
        controls.push(
          <Button
            key="cancelDownload"
            leftIcon={<Spinner size="sm" />}
            onClick={() => cancelDownload()}
          >
            Cancel download
          </Button>
        )
        if (hasCountDisplay(downloader)) {
          const { downloadedCount, totalCount } = downloader

          if (totalCount === 0) {
            status = "..."
          } else {
            const durationDownloaded =
              Number(new Date()) - Number(downloadStartTime)
            const downloadedR = downloadedCount / totalCount
            const remainingDownloadTime =
              durationDownloaded / downloadedR - durationDownloaded

            const remainingDownloadTimeDisplay = Number.isFinite(
              remainingDownloadTime
            )
              ? remainingDownloadTime > 60000
                ? `${Math.floor(remainingDownloadTime / 6e4)} min. remaining`
                : `${Math.floor(remainingDownloadTime / 1000)} sec. remaining`
              : "..."

            status = `Downloading data: ${downloadedCount}/${totalCount} (${Math.floor(
              downloadedR * 100
            )}%, ${remainingDownloadTimeDisplay})`
          }
        } else {
          status = "Downloading data"
        }
      } else {
        controls.push(
          <Button
            key="download"
            leftIcon={<DownloadIcon />}
            onClick={() => download()}
          >
            Download
          </Button>
        )
        status = "Not downloaded"
      }
    } else {
      status = `Data updated as of ${mdate}`

      controls.push(
        <DeleteDataConfirmationModal
          key="clearData"
          onConfirm={() => clearData()}
        />
      )
    }
  }

  return (
    <Tr>
      <Td>{name}</Td>
      <Td>{status}</Td>
      <Td>{controls}</Td>
    </Tr>
  )
}

function hasCountDisplay(downloader: {}): downloader is {
  downloadedCount: number
  totalCount: number
} {
  return "downloadedCount" in downloader && "totalCount" in downloader
}
