import { ChakraProvider } from "@chakra-ui/react"
import { JmdictDatasetManager } from "components/jmdict/dataset"
import "core-js/features/string/replace-all"
import { AppProps } from "next/app"
import React from "react"
import { Title } from "../components/title"
import { WanikaniDictionaryDownloader } from "../components/wanikani/wanikani-dictionary-downloader"
import "../styles/globals.css"
import theme from "../theme"

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ChakraProvider theme={theme}>
      <JmdictDatasetManager>
        <WanikaniDictionaryDownloader>
          <meta
            name="viewport"
            content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no, user-scalable=no, viewport-fit=cover"
          />
          {/* Default title */}
          <Title />
          <Component {...pageProps} />
        </WanikaniDictionaryDownloader>
      </JmdictDatasetManager>
    </ChakraProvider>
  )
}

export default MyApp
