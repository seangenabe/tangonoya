import { extendTheme } from "@chakra-ui/react"

export default extendTheme({
  layerStyles: {
    spaced: {
      gap: "0.5em",
    },
    faded: {
      opacity: "0.5",
    },
  },

  config: {
    initialColorMode: "dark",
    useSystemColorMode: false,
  },
})
