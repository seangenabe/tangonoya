import { toTypescriptCode } from "@schema-kit/tscodegen"
import { writeFileSync } from "fs"
import { entryCollection } from "./types"

const entryCollectionCode = toTypescriptCode(entryCollection)

writeFileSync("types.gen.ts", entryCollectionCode)
