import test, { ExecutionContext } from "ava"
import Ajv, { AnySchema } from "ajv"
import { Schema } from "@schema-kit/common"
import { toJsonSchema } from "@schema-kit/json-schema"
import { entry } from "./types"

const validate = (schema: Schema, t: ExecutionContext<unknown>) => {
  const jsonSchema = toJsonSchema(schema)
  const ajv = new Ajv()
  const v = ajv.validateSchema(jsonSchema as AnySchema)
  t.true(v)
}

test("validate schema", (t) => {
  validate(entry, t)
})
