import {
  array,
  boolean,
  buildConst,
  buildNull,
  integer,
  object,
  Schema,
  string,
  stringEnum,
  union,
} from "@schema-kit/common"
import { ObjectBuilderOptions } from "@schema-kit/common/src/object"
import "@schema-kit/json-schema"

export const subjectType = stringEnum(
  ["kanji", "radical", "vocabulary"] as const,
  {
    title: "SubjectType",
  }
)

export const wkDate = string({ title: "WKDate", description: "WaniKani Date" })

export const meaning = object(
  {
    meaning: string(),
    primary: boolean(),
    accepted_answer: boolean(),
  },
  { title: "Meaning" }
)

export const auxiliaryMeaning = object(
  {
    meaning: string(),
    type: stringEnum(["whitelist", "blacklist"] as const),
  },
  { title: "AuxiliaryMeaning" }
)

export const baseSubject = {
  auxiliary_meanings: array(auxiliaryMeaning),
  created_at: wkDate,
  document_url: string(),
  hidden_at: union([buildNull(), wkDate]),
  lesson_position: integer(),
  level: integer(),
  meaning_mnemonic: string(),
  meanings: array(meaning),
  slug: string(),
  spaced_repetition_system_id: integer(),
}

export const baseCharacterImage = {
  url: string(),
  content_type: string(),
}

export const pngCharacterImage = object(
  {
    ...baseCharacterImage,
    content_type: buildConst("image/png"),
    metadata: object({
      color: string(),
      dimensions: string(),
      style_name: string(),
    }),
  },
  { title: "PngCharacterImage" }
)

export const svgCharacterImage = object(
  {
    ...baseCharacterImage,
    content_type: buildConst("image/svg+xml"),
    metadata: object({
      inline_styles: boolean(),
    }),
  },
  { title: "SvgCharacterImage" }
)

export const characterImage = union([pngCharacterImage, svgCharacterImage], {
  title: "CharacterImage",
})

export const radical = object(
  {
    ...baseSubject,
    amalgamation_subject_ids: array(integer()),
    characters: union([string(), buildNull()]),
    character_images: array(characterImage),
  },
  { title: "Radical" }
)

export const baseReading = {
  reading: string(),
  primary: boolean(),
  accepted_answer: boolean(),
}

export const reading = object(baseReading, { title: "Reading" })

export const kanjiReading = object(
  {
    ...baseReading,
    type: stringEnum(["kunyomi", "nanori", "onyomi"] as const),
  },
  { title: "KanjiReading" }
)

export const kanji = object(
  {
    ...baseSubject,
    characters: string(),
    amalgamation_subject_ids: array(integer()),
    component_subject_ids: array(integer()),
    readings: array(kanjiReading),
    visually_similar_subject_ids: array(integer()),
  },
  { title: "Kanji" }
)

export const contextSentence = object(
  {
    en: string(),
    ja: string(),
  },
  { title: "ContextSentence" }
)

export const pronunciationAudio = object(
  {
    url: string(),
    content_type: stringEnum(["audio/mpeg", "audio/ogg"] as const),
    metadata: object({
      gender: string(),
      source_id: integer(),
      pronunciation: string(),
      voice_actor_id: integer(),
      voice_actor_name: string(),
      voice_description: string(),
    }),
  },
  { title: "PronunciationAudio" }
)

export const vocabulary = object(
  {
    ...baseSubject,
    characters: string(),
    component_subject_ids: array(integer()),
    context_sentences: array(contextSentence),
    meaning_mnemonic: string(),
    parts_of_speech: array(string()),
    pronunciation_audios: array(pronunciationAudio),
    readings: array(reading),
    reading_mnemonic: string(),
  },
  { title: "Vocabulary" }
)

export const subject = union([radical, kanji, vocabulary], {
  title: "Subject",
})

export const entry = object(
  {
    id: integer(),
    data_updated_at: string(),
    url: string(),
    data: subject,
    object: subjectType,
  },
  { title: "Entry" }
)

export const collection = (schema: Schema, opts?: ObjectBuilderOptions) =>
  object(
    {
      url: string(),
      pages: object({
        per_page: integer(),
        next_url: union([string(), buildNull()]),
        previous_url: union([string(), buildNull()]),
      }),
      total_count: integer(),
      data_updated_at: wkDate,
      data: array<any>(schema),
    },
    { title: "Collection", ...opts }
  )

export const entryCollection = collection(entry, { title: "EntryCollection" })
