import {
  CommonDatabaseUpdateOpts,
  paths,
  _mergeDefaultConfig,
} from "@tangonoya/common"
import { compare, Operation } from "fast-json-patch"
import { createWriteStream } from "fs"
import { mkdir, writeFile } from "fs/promises"
import got from "got"
import { Builder } from "lunr"
import { ClientSession, Collection } from "mongodb"
import pThrottle from "p-throttle"
import createLogger from "pino"
import { Entry, EntryCollection, SubjectType, Vocabulary } from "./types.gen"

type WkDate = string

const DB_WRITE_CONCURRENCY = 16

_mergeDefaultConfig({ wanikani: { apiKey: "" } })

export class WaniKaniApi {
  updated_after: WkDate = ""

  constructor(private apiKey: string, private log = createLogger()) {}

  fetchCollectionPageUrl = pThrottle(
    this._fetchCollectionPageUrl.bind(this),
    60,
    60000
  ) as WaniKaniApi["_fetchCollectionPageUrl"]

  private async _fetchCollectionPageUrl(url: string) {
    const u = new URL(url)
    const searchParams = u.searchParams
    if (!searchParams.has("updated_after") && this.updated_after) {
      searchParams.set("updated_after", this.updated_after)
    }

    const body: EntryCollection = await got(url, {
      headers: {
        authorization: `Bearer ${this.apiKey}`,
        "WaniKani-Revision": "20170710",
      },
      searchParams,
    }).json()

    this.log.info({
      msg: "got collection",
      length: body.data.length,
      totalLength: body.total_count,
    })
    return body
  }

  async *fetchCollection({
    endpoint,
    url: startUrl,
  }: {
    endpoint?: string
    url?: string
  }) {
    let url: string | null | undefined = startUrl
    if (endpoint == null && url == null) {
      throw new Error("endpoint or url must be defined")
    }
    if (endpoint != null) {
      url = `https://api.wanikani.com/v2/${endpoint}`
    }
    if (url == null) {
      throw new Error("url must be defined")
    }

    do {
      this.log.info({ url }, "Fetching URL")
      const collection: EntryCollection = await this.fetchCollectionPageUrl(url)
      yield { collection, url }
      url = collection.pages.next_url
    } while (url)
  }
}

export async function updateDatabase({
  collection,
  metaCollection,
  patchesCollection,
  searchCollection,
  apiKey,
  logger = { enabled: false },
  session,
}: UpdateDatabaseOpts) {
  const meta = await metaCollection.findOne<WanikaniMeta>(
    { _id: "wanikani" },
    { session }
  )
  const log = createLogger(logger)
  const api = new WaniKaniApi(apiKey, log)

  let inputOpts: { endpoint?: string; url?: string } = {
    endpoint: "subjects",
  }
  if (meta != null) {
    api.updated_after = meta.mdate
    if (meta.retryUrl) {
      inputOpts = { url: meta.retryUrl }
    }
  }

  let updatedAt = meta?.mdate ?? ""

  const insertSearchEntry = async function insertSearchEntry(
    e: number,
    h: string,
    session: ClientSession
  ) {
    await searchCollection.updateOne(
      { _id: `${h}!${e}` },
      {
        $set: { h, e },
      },
      { upsert: true, session }
    )
  }

  const insertSearchEntriesForWanikaniEntry = async function insertSearchEntriesForWanikaniEntry(
    entry: Entry,
    session: ClientSession
  ) {
    if (isVocabulary(entry)) {
      await insertSearchEntry(entry.id, entry.data.characters, session)
    }
  }

  for await (const item of api.fetchCollection(inputOpts)) {
    await metaCollection.updateOne(
      { _id: "wanikani" },
      { $set: { url: item.url } },
      { upsert: true, session }
    )

    updatedAt = item.collection.data_updated_at

    if (item.collection.data.length === 0) {
      break
    }

    for await (const entry of item.collection.data) {
      await session.withTransaction(async (session) => {
        const newEntry = { _id: entry.id, ...entry }

        const { value: oldEntry = {} } = await collection.findOneAndReplace(
          { _id: entry.id },
          newEntry,
          { upsert: true, session }
        )

        if (meta == null) {
          // New entry, new database
          await insertSearchEntriesForWanikaniEntry(entry, session)
          return
        }

        if (oldEntry == null) {
          // New entry, patch old entry
          await patchesCollection.insertOne(
            {
              mdate: item.collection.data_updated_at,
              id: newEntry.id,
              newDocument: entry,
            },
            { session }
          )
          log.info({ id: entry.id }, "Got new entry")
          await insertSearchEntriesForWanikaniEntry(entry, session)
          return
        }

        // Update existing entry

        await searchCollection.deleteMany({ e: newEntry.id }, { session })
        await insertSearchEntriesForWanikaniEntry(newEntry, session)

        const operations = compare(oldEntry, newEntry)

        if (operations.length === 0) {
          return
        }

        await patchesCollection.insertOne(
          {
            mdate: newEntry.data_updated_at,
            id: newEntry.id,
            operations,
          },
          { session }
        )

        log.info(`Created patch for entry ${entry.id}`)
      })
    }

    log.info({ url: item.url, updatedAt }, "Updated items")
  }
  log.info("done updating items", updatedAt)

  await metaCollection.updateOne(
    { _id: "wanikani" },
    { $set: { mdate: updatedAt, retryUrl: "" } },
    { upsert: true, session }
  )

  log.info("writing gloss indexes")
  const builder = new Builder()
  builder.field("glosses")

  for await (const entry of collection.find({}, { session })) {
    builder.add({
      id: entry.id,
      // https://stackoverflow.com/a/43562885/1149962
      glosses: entry.data.meanings.map((x) => x.meaning).join(" "),
    })
  }

  const index = builder.build()

  await mkdir(`${paths.data}/wanikani`, { recursive: true })
  const outFile = `${paths.data}/wanikani/gloss-index.json`
  await writeFile(outFile, JSON.stringify(index))
  log.info(`index written to ${outFile}`)

  log.info("writing all entries file")

  const entriesStream = createWriteStream(
    `${paths.data}/wanikani/entries.ndjson`,
    { encoding: "utf-8" }
  )

  for await (const entry of collection.find({})) {
    entriesStream.write(`${JSON.stringify(entry)}\n`)
  }
  entriesStream.end()
  await writeFile(
    `${paths.data}/wanikani/meta.json`,
    JSON.stringify({ mdate: updatedAt }),
    { encoding: "utf-8" }
  )
  log.info("done writing all entries")
}

function isVocabulary(
  entry: Entry
): entry is Entry & { object: SubjectType.vocabulary; data: Vocabulary } {
  return entry.object === SubjectType.vocabulary
}

export interface UpdateDatabaseOpts
  extends CommonDatabaseUpdateOpts<Entry & { _id: number }> {
  apiKey: string
  patchesCollection: Collection<EntryPatch>
  searchCollection: Collection<WanikaniSearchEntry>
}

export interface WanikaniSearchEntry {
  _id: string
  e: number
  h: string
}

export interface EntryPatch {
  mdate: string
  id: number
  operations?: Operation[]
  newDocument?: Entry
}

export interface WanikaniMeta {
  mdate: WkDate
  retryUrl: string
}
