import "@tangonoya/common"

declare module "@tangonoya/common" {
  export interface Config {
    wanikani: {
      apiKey: string
    }
  }
}
