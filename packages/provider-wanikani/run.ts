#!/usr/bin/env node

import { getConfig } from "@tangonoya/common"
import { connect } from "mongodb"
import { EntryPatch, updateDatabase, WanikaniSearchEntry } from "."
import { Entry } from "./types.gen"

export async function runUpdateDatabase() {
  const config = getConfig()

  if (!config.wanikani.apiKey) {
    throw new Error("API key required.")
  }

  const client = await connect(config.db)

  try {
    const db = client.db()
    const collection = db.collection<Entry & { _id: number }>("wanikani")
    const patchesCollection = db.collection<EntryPatch>("wanikani-patches")
    const searchCollection = db.collection<WanikaniSearchEntry>("jmdict-search")
    const metaCollection = db.collection("meta")

    await searchCollection.createIndex({ h: 1, e: 1 })
    await searchCollection.createIndex({ e: 1 })

    await client.withSession(async (session) => {
      await patchesCollection.createIndex({ mdate: 1 })

      await updateDatabase({
        apiKey: config.wanikani.apiKey,
        session,
        collection,
        metaCollection,
        patchesCollection,
        searchCollection,
        transactionOptions: {
          readConcern: { level: "majority" },
          writeConcern: { w: "majority" },
        },
        logger: { enabled: true },
      })
    })
  } finally {
    await client.close()
  }
}

if (require.main === module) {
  runUpdateDatabase().catch((err) => {
    console.error(err)
    process.exit(1)
  })
}
