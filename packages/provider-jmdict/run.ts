#!/usr/bin/env node

import { getConfig } from "@tangonoya/common"
import { Collection, connect, Db } from "mongodb"
import { updateDatabase, JmdictPatch, JmdictSearchEntry, JmdictMeta } from "."
import { Jmdict } from "./types.gen"

export async function runUpdateDatabase() {
  const config = getConfig()

  const client = await connect(config.db)

  try {
    const db = client.db()
    const collection = await ensureCollectionExists<
      Jmdict & { _id: number; mdate: string }
    >(db, "jmdict")
    const patchesCollection = await ensureCollectionExists<JmdictPatch>(
      db,
      "jmdict-patches"
    )
    const searchCollection = await ensureCollectionExists<JmdictSearchEntry>(
      db,
      "jmdict-search"
    )
    const metaCollection = await ensureCollectionExists<JmdictMeta>(db, "meta")

    await client.withSession(async (session) => {
      await patchesCollection.createIndex({ mdate: 1 })

      await updateDatabase({
        session,
        collection,
        metaCollection,
        patchesCollection,
        searchCollection,
        logger: { enabled: true },
        transactionOptions: {
          readConcern: { level: "majority" },
          writeConcern: { w: "majority" },
        },
        gunzipThenRsyncOpts: config.jmdict.gunzipThenRsyncOpts,
      })
    })
  } finally {
    await client.close()
  }
}

async function ensureCollectionExists<T = unknown>(
  db: Db,
  collectionName: string
): Promise<Collection<T>> {
  const collections = await db
    .listCollections({ name: collectionName })
    .toArray()
  if (collections.length === 0) {
    return await db.createCollection(collectionName)
  }
  return db.collection(collectionName)
}

if (require.main === module) {
  runUpdateDatabase().catch((err) => {
    console.error(err)
    process.exit(1)
  })
}
