export function ensureMapValue<K, V>(map: Map<K, V> , key: K, valueFn: (key: K) => V ) {
  if (!map.has(key)) {
    const value = valueFn(key)
    map.set(key, value)
    return value
  }
  return map.get(key)!
}
