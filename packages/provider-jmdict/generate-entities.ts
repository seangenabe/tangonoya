import { writeFile } from "fs/promises"
import got from "got/dist/source"
import { JmdictTransform } from "jmdict-streaming-parser"
import { pipeline } from "stream"
import { createGunzip } from "zlib"

const BASE_URL = "http://ftp.edrdg.org/pub/Nihongo"

async function generateEntities() {
  const transform = new JmdictTransform()
  const source = got.stream("JMdict_e.gz", { prefixUrl: BASE_URL })
  pipeline(source, createGunzip(), transform, () => undefined)
  let groupedEntities: ReadonlyMap<
    string,
    readonly { name: string; value: string }[]
  >

  for await (const entry of transform) {
    if (entry.type === "groupedEntities") {
      groupedEntities = entry.data
      break
    }
  }

  const groupedEntitiesObj = groupedEntities!
    ? Object.fromEntries(groupedEntities!)
    : {}

  await writeFile("entities.json", JSON.stringify(groupedEntitiesObj, null, 2))
}

;(async () => {
  try {
    await generateEntities()
  } catch (err) {
    console.error(err)
    process.exit(1)
  }
})()
