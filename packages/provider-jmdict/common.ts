import "@tangonoya/common"
import { GunzipThenRsyncOpts } from "gunzip-then-rsync"

declare module "@tangonoya/common" {
  export interface Config {
    jmdict: {
      gunzipThenRsyncOpts: GunzipThenRsyncOpts
    }
  }
}
