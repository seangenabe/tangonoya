import { toTypescriptCode } from "@schema-kit/tscodegen"
import { writeFile } from "fs/promises"
;(async () => {
  try {
    const { jmdictSchema, sourceJmdictEntry } = await import("./types")

    const jmdictSchemaCode = toTypescriptCode(jmdictSchema)
    const sourceJmdictSchemaCode = toTypescriptCode(sourceJmdictEntry)

    await writeFile(
      "types.gen.ts",
      `${jmdictSchemaCode}

    ${sourceJmdictSchemaCode}`
    )
  } catch (err) {
    console.error(err)
    process.exit(1)
  }
})()
