import { toArray } from "@reactivex/ix-esnext-cjs/asynciterable/toarray"
import {
  CommonDatabaseUpdateOpts,
  omitNullOrUndefined as ou,
  paths,
} from "@tangonoya/common"
import { ok } from "assert"
import { map as aiMap } from "async-iterable-map"
import { compare, Operation } from "fast-json-patch"
import { createReadStream, createWriteStream, WriteStream } from "fs"
import { mkdir, writeFile } from "fs/promises"
import { gunzipThenRsync, GunzipThenRsyncOpts } from "gunzip-then-rsync"
import { JmdictTransform } from "jmdict-streaming-parser"
import { Builder } from "lunr"
import { Collection } from "mongodb"
import createLogger from "pino"
import { createInterface } from "readline"
import { ensureMapValue } from "./ensure-map-value"
import { Jmdict, Sense, SourceJmdictEntry } from "./types.gen"

const BASE_URL = "http://ftp.edrdg.org/pub/Nihongo"

const DB_WRITE_CONCURRENCY = 16
const FS_WRITE_CONCURRENCY = 4
const defaultGunzipThenRsyncOpts: (baseUrl: string) => GunzipThenRsyncOpts = (
  baseUrl
) => ({
  gunzipUrl: `${baseUrl}/JMdict_e.gz`,
  rsyncSource: "ftp.edrdg.org::nihongo/JMdict_e",
})

export async function updateDatabase({
  session,
  collection,
  metaCollection,
  patchesCollection,
  searchCollection,
  logger = { enabled: false },
  baseUrl = BASE_URL,
  gunzipThenRsyncOpts,
  transactionOptions = {},
}: UpdateDatabaseOpts) {
  const log = createLogger(logger)
  const transform = new JmdictTransform()
  const languageCodesUsed = new Set<string>(["eng"])
  const indexBuilders: Map<string, Builder> = new Map()

  let mdate: string

  const source = gunzipThenRsync({
    ...defaultGunzipThenRsyncOpts(baseUrl),
    ...gunzipThenRsyncOpts,
  })

  source.pipe(transform)

  await searchCollection.createIndex({ h: 1, e: 1 }, { session })
  await searchCollection.createIndex({ e: 1 }, { session })

  let cancel = false
  const meta = await metaCollection.findOne({ _id: "jmdict" }, { session })

  log.info("updating entries")

  const generateEntries = async function* () {
    for await (const entry of transform) {
      if (entry.type === "mdate") {
        mdate = entry.data
        log.info({ mdate }, "got mdate")

        if (meta?.mdate === entry.data) {
          cancel = true
          log.info({ mdate, newMdate: entry.data }, "mdate matched, aborting")
          return
        }
      } else if (entry.type === "node") {
        if (cancel) return

        const newEntry = normalizeJmdictEntry(
          (entry.data as unknown) as SourceJmdictEntry
        )

        yield newEntry
      }
    }
  }

  await aiMap(
    generateEntries(),
    async (entry) => {
      await session.withTransaction(async (session) => {
        const insertSearchEntry = async function insertSearchEntry(
          e: number,
          h: string
        ) {
          await searchCollection.updateOne(
            {
              _id: `${h}!${e}`,
            },
            {
              $set: {
                h,
                e,
              },
            },
            { upsert: true, session }
          )
        }

        const insertSearchEntriesForJmdictEntry = async function insertSearchEntryForJmdictEntry(
          entry: Jmdict
        ) {
          await Promise.all([
            ...(entry.k_ele ?? []).map((k) =>
              insertSearchEntry(entry.ent_seq, k.keb)
            ),
            ...entry.r_ele.map((r) => insertSearchEntry(entry.ent_seq, r.reb)),
          ])
        }

        const newEntry = { _id: entry.ent_seq, mdate, ...entry }

        const existingEntry = await collection.findOne({ _id: entry.ent_seq })

        if (existingEntry && existingEntry.mdate === mdate) {
          log.info(`mdate matched, skipping ${entry.ent_seq}`)
          return
        }

        const glossesByLanguage: Map<string, string[]> = new Map()
        for (const sense of entry.sense) {
          for (const gloss of sense.gloss ?? []) {
            if (gloss.lang) {
              languageCodesUsed.add(gloss.lang)
            }
            if (gloss.value) {
              ensureMapValue(
                glossesByLanguage,
                gloss.lang ?? "eng",
                () => []
              ).push(gloss.value)
            }
          }

          for (const lsource of sense.lsource ?? []) {
            if (lsource.lang) {
              languageCodesUsed.add(lsource.lang)
            }
          }
        }

        if (glossesByLanguage.size !== 0) {
          for (const [lang, glosses] of glossesByLanguage) {
            ensureMapValue(indexBuilders, lang, () => {
              const builder = new Builder()

              // builder.pipeline.add(trimmer, stopWordFilter, stemmer)

              // builder.searchPipeline.add(stemmer)

              builder.field("glosses")
              return builder
            }).add({
              id: entry.ent_seq,
              // https://stackoverflow.com/a/43562885/1149962
              glosses: glosses.join(" "),
            })
          }
        }

        await collection.findOneAndReplace({ _id: entry.ent_seq }, newEntry, {
          upsert: true,
          returnOriginal: true,
          session,
        })

        if (meta == null) {
          // New entry, new database
          await insertSearchEntriesForJmdictEntry(entry)
          return
        }

        if (existingEntry == null) {
          // New entry, patch old entry
          await patchesCollection.insertOne(
            {
              mdate,
              ent_seq: newEntry.ent_seq,
              newDocument: entry,
            },
            { session }
          )
          log.info({ent_seq: newEntry.ent_seq},"Got new entry")
          await insertSearchEntriesForJmdictEntry(newEntry)
          return
        }

        // Update existing entry

        await searchCollection.deleteMany({ e: newEntry.ent_seq }, { session })
        await insertSearchEntriesForJmdictEntry(newEntry)

        const operations = compare(
          omitMdate(existingEntry),
          omitMdate(newEntry)
        )

        if (operations.length === 0) {
          log.info(`No change, skipping entry ${entry.ent_seq}`)
          return
        }

        log.info("Patching entry")
        await patchesCollection.insertOne(
          {
            mdate,
            ent_seq: newEntry.ent_seq,
            operations,
          },
          { session }
        )

        log.info(`Created patch for entry ${entry.ent_seq}`)
      }, transactionOptions)
    },
    { concurrency: DB_WRITE_CONCURRENCY }
  )

  ok(mdate!)
  log.info("updating entries done")

  const languages = await toArray(getUsedLanguages(languageCodesUsed))
  await metaCollection.updateOne(
    { _id: "jmdict" },
    { $set: { languages } },
    { upsert: true, session }
  )

  await mkdir(`${paths.data}/jmdict`, { recursive: true })

  if (!cancel) {
    log.info("writing gloss indexes", { languagesCount: indexBuilders.size })
    await aiMap(
      Array.from(indexBuilders),
      async ([lang, builder]) => {
        const index = builder.build()
        const outFile = `${paths.data}/jmdict/gloss-index-${lang}.json`
        await writeFile(outFile, JSON.stringify(index))
        log.info(`index written to ${outFile}`)
      },
      { concurrency: FS_WRITE_CONCURRENCY }
    )

    await metaCollection.updateOne(
      { _id: "jmdict" },
      { $set: { mdate } },
      { upsert: true, session }
    )
    log.info({ mdate }, "mdate saved")
  }

  log.info("writing all entries file")

  const entriesStream = createWriteStream(
    `${paths.data}/jmdict/entries.ndjson`,
    { encoding: "utf-8" }
  )
  const entriesLangStreamByLanguage = new Map<string, WriteStream>()
  const languagesUsed = new Set<string>()

  for await (const { _id, mdate, ...entry } of collection
    .find({})
    .sort({ _id: 1 })) {
    const entryBase = {
      ...entry,
      sense: undefined,
    }

    entriesStream.write(`${JSON.stringify(entryBase)}\n`)

    // Collect [sense index, sense] by language

    const sensesByLanguage = new Map<string, [number, Sense][]>()

    for (const [senseIndex, sense] of entry.sense.entries()) {
      if (sense.gloss) {
        const languageCodes = sense.gloss.map((gloss) => gloss.lang ?? "eng")
        for (const languageCode of languageCodes) {
          languagesUsed.add(languageCode)
          ensureMapValue(sensesByLanguage, languageCode, () => []).push([
            senseIndex,
            sense,
          ])
        }
      }
    }

    for (const [lang, senses] of sensesByLanguage) {
      const entryLang = {
        ent_seq: entry.ent_seq,
        senses,
      }

      const outputStream = ensureMapValue(
        entriesLangStreamByLanguage,
        lang,
        (lang) =>
          createWriteStream(`${paths.data}/jmdict/entries-${lang}.ndjson`, {
            encoding: "utf-8",
          })
      )

      // Save [senseIndex, sense] with ent_seq
      outputStream.write(`${JSON.stringify(entryLang)}\n`)
    }
  }
  entriesStream.end()
  for (const stream of entriesLangStreamByLanguage.values()) {
    stream.end()
  }

  const metaJson: JmdictMetaJson = {
    mdate,
    languagesUsed: Array.from(languagesUsed),
  }
  await writeFile(`${paths.data}/jmdict/meta.json`, JSON.stringify(metaJson))

  log.info("done")
}

function normalizeJmdictEntry(src: SourceJmdictEntry): Jmdict {
  const out: Partial<Jmdict> = {
    ent_seq: Number(src.ent_seq[0]),
  }
  if (src.k_ele) {
    out.k_ele = src.k_ele.map((x) =>
      ou({
        ...x,
        keb: x.keb[0],
      })
    ) as any
  }
  if (src.r_ele) {
    out.r_ele = src.r_ele.map((x) =>
      ou({
        ...x,
        reb: x.reb[0],
        re_nokanji: x.re_nokanji?.[0],
      })
    ) as any
  }
  out.sense = src.sense?.map(
    (x) =>
      ou({
        ...(x as Omit<typeof x, "lsource" | "gloss">),
        lsource: x.lsource?.map((lsource) =>
          ou({
            value: lsource.$text!,
            ls_type: lsource.ls_type,
            ls_wasei: lsource.ls_wasei === "y" ? (true as const) : undefined,
            lang: lsource["xml:lang"],
          })
        ),
        gloss: x.gloss?.map((gloss) =>
          typeof gloss === "string"
            ? { value: gloss }
            : ou({
                value: gloss.$text!,
                lang: gloss["xml:lang"],
                g_type: gloss.g_type,
                g_gend: gloss.g_gend,
                pri: gloss.pri,
              })
        ),
      }) as any
  )

  return out as Jmdict
}

export async function* getAllLanguages() {
  const stream = createReadStream(`${__dirname}/languages-iso-639-2.csv`)
  const rl = createInterface({
    input: stream,
    crlfDelay: Infinity,
  })

  for await (const line of rl) {
    const [, first, rest] = line.match(/([^,]*),(.*)/)!
    yield [first, rest]
  }
}

export async function* getUsedLanguages(languages: ReadonlySet<string>) {
  const allLanguages = getAllLanguages()
  for await (const [code, description] of allLanguages) {
    if (languages.has(code)) {
      yield { code, description }
    }
  }
}

function omitMdate<T extends { mdate: string }>(obj: T) {
  const { mdate, ...rest } = obj
  return rest
}

export interface UpdateDatabaseOpts
  extends CommonDatabaseUpdateOpts<
    Jmdict,
    JmdictMeta,
    { _id: number; mdate: string }
  > {
  gunzipThenRsyncOpts?: GunzipThenRsyncOpts
  patchesCollection: Collection<JmdictPatch>
  searchCollection: Collection<JmdictSearchEntry>
  baseUrl?: string
}

export interface JmdictMeta {
  _id: "jmdict"
  mdate: string
  languages: { code: string; description: string }[]
}

export interface JmdictSearchEntry {
  _id: string
  e: number
  h: string
}

export interface JmdictPatch {
  mdate: string
  ent_seq: number
  operations?: Operation[]
  newDocument?: Jmdict
}

export interface JmdictMetaJson {
  mdate: string
  languagesUsed: string[]
}

export interface JmdictSenseByLangEntry {
  ent_seq: number
  senses: [number, Sense][]
}
