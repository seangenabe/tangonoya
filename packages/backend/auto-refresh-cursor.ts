import { Cursor } from "mongodb"

const REFRESH_AFTER_DURATION = 1000 * 60 * 9

class CursorRefresher<T> {
  private readonly createCursor: () => Cursor<T>
  private currentCursor!: AsyncIterator<T>
  private cursorLastRefreshed!: Date
  private seenItemsCount = 0

  constructor(createCursor: () => Cursor<T>) {
    this.createCursor = createCursor
    this.refreshCursor()
  }

  refreshCursor() {
    this.currentCursor = this.createCursor()
      .skip(this.seenItemsCount)
      [Symbol.asyncIterator]()
    this.cursorLastRefreshed = new Date()
  }

  private async nextRaw(): Promise<IteratorResult<T>> {
    const ret = await this.currentCursor.next()
    this.seenItemsCount++
    return ret
  }

  async next(): Promise<IteratorResult<T>> {
    if (
      Number(this.cursorLastRefreshed) + REFRESH_AFTER_DURATION >
      Number(new Date())
    ) {
      this.refreshCursor()
    }
    try {
      return await this.nextRaw()
    } catch (err) {
      if (
        err.name === "MongoError" &&
        (err as Error).message.match(/cursor does not exist/)
      ) {
        this.refreshCursor()
        return await this.nextRaw()
      }
      throw err
    }
  }
}

export function autoRefreshCursor<T>(
  createCursor: () => Cursor<T>
): AsyncIterable<T> {
  return {
    [Symbol.asyncIterator]() {
      return new CursorRefresher(createCursor)
    },
  }
}
