import {
  any,
  array,
  buildConst,
  integer,
  object,
  optional,
  string,
  stringEnum,
} from "@schema-kit/common"
import { toJsonSchema } from "@schema-kit/json-schema"
import { ToTypescriptType } from "@schema-kit/typescript"
import { paths } from "@tangonoya/common"
import { WanikaniMeta, WanikaniSearchEntry } from "@tangonoya/provider-wanikani"
import { entry as entrySchema } from "@tangonoya/provider-wanikani/types"
import { Entry } from "@tangonoya/provider-wanikani/types.gen"
import escapeStringRegexp from "escape-string-regexp"
import { FastifyPluginAsync } from "fastify"
import { readFile } from "fs/promises"
import { from, toArray } from "ix/asynciterable"
import { buffer, concatAll, map, skip, take } from "ix/asynciterable/operators"
import { Index, tokenizer } from "lunr"
import { createMatcher } from "./create-matcher"
import { iterableToNdjsonStream } from "./iterable-to-ndjson-stream"
import { streamOrGetAllResults } from "./stream-or-get-all-results"
import {
  paginatedSchema,
  streamOrPaginateResults,
} from "./stream-or-paginate-results"
import { streamOrPaginateSlow } from "./stream-or-paginate-slow"

export const ERR_NOTFOUND = "Entry not found."
const HTTP_OK = 200
const HTTP_NOTFOUND = 404

const entryJsonSchema = toJsonSchema(entrySchema)

export const wanikani: FastifyPluginAsync = async (server) => {
  const db = server.mongo.client.db()
  const entriesCollection = db?.collection<Entry & { _id: number }>("wanikani")
  const metaCollection = db?.collection<WanikaniMeta>("meta")
  const searchCollection = db?.collection<WanikaniSearchEntry>("jmdict-search")
  const patchesCollection = db?.collection<WanikaniPatch>("wanikani-patches")
  const indexStr = await readFile(`${paths.data}/wanikani/gloss-index.json`, {
    encoding: "utf-8",
  })
  const index = Index.load(JSON.parse(indexStr))

  const entriesIdParamsSchema = object({
    id: integer({
      description: "The id field of the entry.",
      minimum: 1,
    }),
  })
  server.get<{ Params: ToTypescriptType<typeof entriesIdParamsSchema> }>(
    "/entries/:id",
    {
      schema: {
        description: "Gets a Wanikani subject with the specified ID.",
        params: toJsonSchema(entriesIdParamsSchema),
        response: {
          [HTTP_OK]: entryJsonSchema,
          [HTTP_NOTFOUND]: toJsonSchema(
            buildConst(ERR_NOTFOUND, {
              description: "The entry with the specified ID does not exist.",
            })
          ),
        },
      },
    },
    async (req, reply) => {
      const entry = await entriesCollection.findOne({
        _id: req.params.id,
      })

      if (entry == null) {
        reply.code(HTTP_NOTFOUND)
        return ERR_NOTFOUND
      }

      return entry
    }
  )

  const entriesQuerystringSchema = object({
    level: optional(integer({ minimum: 1 })),
    characters: optional(string()),
    meaning_starts: optional(string()),
    after: optional(integer()),
  })
  server.get<{
    Querystring: ToTypescriptType<typeof entriesQuerystringSchema>
  }>(
    "/entries",
    {
      schema: {
        description: "Gets all WaniKani entries.",
        response: {
          [HTTP_OK]: toJsonSchema(paginatedSchema(entrySchema)),
        },
        querystring: toJsonSchema(entriesQuerystringSchema),
      },
    },
    async (req, reply) => {
      const meta = await metaCollection.findOne({ _id: "wanikani" })
      const { mdate } = meta!
      reply.header("x-wanikani-mdate", mdate)
      reply.header("access-control-expose-headers", "x-wanikani-mdate")

      const match0: Record<string, unknown> = {}

      const getResult = (idCursor?: number) => {
        match0["_id"] = { $gt: idCursor ?? 0 }
        if (req.query.level != null) {
          match0["data.level"] = req.query.level
        }
        if (req.query.characters != null) {
          match0["data.characters"] = req.query.characters
        }
        if (req.query.meaning_starts != null) {
          match0["data.meanings.meaning"] = {
            $regex: new RegExp(
              `^${escapeStringRegexp(req.query.meaning_starts)}`,
              "i"
            ),
          }
        }

        return entriesCollection.find(match0)
      }

      return await streamOrPaginateResults<
        number,
        "_id",
        Entry & { _id: number }
      >(req, reply, getResult, {
        pageSize: 20,
      })
    }
  )

  const searchQuerystringSchema = object({
    after: optional(
      integer({
        description: "Used for pagination.",
        default: 0,
        examples: [0],
      })
    ),
    q: string({
      description: "Filter entries.",
    }),
    search_mode: stringEnum(["exact", "prefix"] as const),
  })

  server.get<{
    Querystring: ToTypescriptType<typeof searchQuerystringSchema>
  }>(
    "/search",
    {
      schema: {
        description:
          "Search-optimized version of `entries`. Gets all matching WaniKani entries.",
        response: {
          [HTTP_OK]: toJsonSchema(paginatedSchema(entrySchema)),
        },
        querystring: toJsonSchema(searchQuerystringSchema),
      },
    },
    async (req, reply) => {
      const { mdate } = (await metaCollection.findOne({ _id: "wanikani" }))!

      reply.header("x-mdate", mdate)
      reply.header("access-control-expose-headers", "x-mdate")

      const getResults = () => {
        return searchCollection.aggregate([
          {
            $match: {
              h: createMatcher(req.query.q, req.query.search_mode),
            },
          },
          {
            $lookup: {
              from: "wanikani",
              localField: "e",
              foreignField: "_id",
              as: "wanikaniEntry",
            },
          },
          {
            $unwind: {
              path: "$wanikaniEntry",
              preserveNullAndEmptyArrays: false,
            },
          },
          {
            $replaceRoot: {
              newRoot: "$wanikaniEntry",
            },
          },
        ])
      }

      return await streamOrPaginateSlow(req, reply, {
        getResults,
        idSelector: ({ _id }) => _id,
      })
    }
  )

  const searchGlossesSchema = object({
    after: optional(
      integer({
        description: "Used for pagination.",
        default: 0,
        examples: [0],
      })
    ),
    q: string({
      description: "Filter entries.",
    }),
  })

  server.get<{ Querystring: ToTypescriptType<typeof searchGlossesSchema> }>(
    "/search-glosses",
    {
      schema: {
        description: "Search entries from dictionary glosses.",
        response: {
          [HTTP_OK]: toJsonSchema(paginatedSchema(entrySchema)),
        },
        querystring: toJsonSchema(searchGlossesSchema),
      },
    },
    async (req, reply) => {
      const { mdate } = (await metaCollection.findOne({ _id: "jmdict" }))!

      reply.header("x-mdate", mdate)
      reply.header("access-control-expose-headers", "x-mdate")

      const indexResults = index.query((query) => {
        const tokens = tokenizer(req.query.q)
        for (const token of tokens) {
          query.term(token, {
            fields: ["glosses"],
            // wildcard: Query.wildcard.TRAILING,
          })
        }
      })

      const getEntriesFromIds = async (ids: number[]) => {
        if (ids.length === 0) {
          return []
        }
        const entries = await entriesCollection
          .find({
            _id: {
              $in: ids,
            },
          })
          .toArray()
        const entriesById = new Map(entries.map((entry) => [entry.id, entry]))

        return ids.map((id) => entriesById.get(id)!)
      }

      if (req.headers.accept === "application/x-ndjson") {
        reply.header("content-type", "application/x-ndjson")

        const source = from(indexResults).pipe(
          map((r) => Number(r.ref)),
          buffer(20),
          map(getEntriesFromIds),
          concatAll()
        )

        reply.send(iterableToNdjsonStream(source))
        return
      }
      const after = req.query.after ?? 0
      const ids = await toArray(
        from(indexResults).pipe(
          skip(after),
          take(20),
          map((r) => Number(r.ref))
        )
      )
      const results = await getEntriesFromIds(ids)
      const hasNext = results.length === 20

      return {
        items: results,
        hasNext,
        nextPage: after + results.length,
      }
    }
  )

  const patchesQueryStringSchema = object({
    after: string({
      description: "The exclusive lower bound for filtering patches.",
    }),
  })

  const patchSchema = object({
    mdate: string(),
    id: integer(),
    operations: optional(array(any({ title: "Operation" }))),
    newDocument: optional(entrySchema),
  })

  server.get<{
    Querystring: ToTypescriptType<typeof patchesQueryStringSchema>
  }>(
    "/patches",
    {
      schema: {
        description:
          "Gets all changes to JMdict entries after the specified date.",
        querystring: toJsonSchema(patchesQueryStringSchema),
        response: {
          [HTTP_OK]: toJsonSchema(array(patchSchema)),
        },
      },
    },
    async (req, reply) => {
      const results = patchesCollection
        .find({
          mdate: { $gt: req.query.after },
        })
        .sort([["mdate", 1]])

      return await streamOrGetAllResults(req, reply, results)
    }
  )

  const statsSchema = object({
    totalCount: integer(),
    mdate: string(),
  })

  server.get(
    "/stats",
    {
      schema: {
        response: {
          [HTTP_OK]: toJsonSchema(statsSchema),
        },
      },
    },
    async () => {
      const meta = await metaCollection.findOne({ _id: "wanikani" })

      return {
        mdate: meta!.mdate,
        totalCount: await entriesCollection.countDocuments(),
      }
    }
  )

  //
  ;(async () => {
    await server.ready()
    await entriesCollection.createIndexes(
      [
        { "data.level": 1 },
        { "data.characters": 1 },
        { "data.meanings.meaning": 1 },
      ].map((key) => ({ key }))
    )
  })()
}
