import anyTest, { TestInterface } from "ava"
import { createServer, Server } from "."
import { ERR_NOTFOUND } from "./jmdict"

const HTTP_BADREQUEST = 400
const HTTP_NOTFOUND = 404

const test = anyTest as TestInterface<{ server: Server }>

test.beforeEach(async (t) => {
  t.context.server = await createServer({ logger: false })
})

test("entries/:id", async (t) => {
  const response = await t.context.server.inject({
    method: "GET",
    path: "/jmdict/entries/1577200",
  })

  t.is(JSON.parse(response.body).k_ele[0].keb, "可愛い")
})

test("entries/:id invalid entry id", async (t) => {
  const response = await t.context.server.inject({
    method: "GET",
    path: "/jmdict/entries/1",
  })

  t.is(response.statusCode, HTTP_NOTFOUND)
  t.is(response.body, ERR_NOTFOUND)
})

test("entries/:id validation", async (t) => {
  for (const input of ["a", "-1"]) {
    const response = await t.context.server.inject({
      method: "GET",
      path: `/jmdict/entries/${input}`,
    })

    t.is(response.statusCode, HTTP_BADREQUEST)
  }
})

test("entries", async (t) => {
  const response = await t.context.server.inject({
    method: "GET",
    path: "/jmdict/entries",
  })

  const body = JSON.parse(response.body)

  t.true(Array.isArray(body.items))
  t.is(typeof body.items[0].ent_seq, "number")
  t.is(typeof body.hasNext, "boolean")
  t.is(typeof body.nextPage, "number")
})
