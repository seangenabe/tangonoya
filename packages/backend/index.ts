import { getConfig } from "@tangonoya/common"
import fastify, { FastifyServerOptions } from "fastify"
import FastifyCors from "fastify-cors"
import FastifyMongodb from "fastify-mongodb"
import FastifySwagger from "fastify-swagger"
import { MongoClient } from "mongodb"
import { jmdict } from "./jmdict"
import { wanikani } from "./wanikani"

const pkg = require("./package")
const homepage = pkg.homepage as string
const version = pkg.version as string
const description = pkg.description as string

const config = getConfig()

export async function createServer(
  opts: FastifyServerOptions = { logger: true }
) {
  const client = await MongoClient.connect(config.db)

  const server = fastify({
    ajv: { customOptions: { coerceTypes: "array" } },
    ...opts,
  })

  server.register(FastifyCors, {
    origin: "*",
  })

  server.register(FastifyMongodb, {
    client,
    forceClose: true,
  })

  server.register(FastifySwagger, {
    swagger: {
      info: {
        title: "Tangonoya backend",
        description,
        version,
      },
      externalDocs: {
        url: homepage,
      },
    },
    exposeRoute: true,
  })

  await server.after()

  server.get("/", { schema: { hide: true } }, async (req, reply) =>
    reply.redirect("/documentation")
  )

  server.register(jmdict, { prefix: "/jmdict" })
  server.register(wanikani, { prefix: "/wanikani" })

  return server
}

type Result<T> = T extends () => Promise<infer R> ? R : never

export type Server = Result<typeof createServer>
