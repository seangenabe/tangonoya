import { FastifyReply, FastifyRequest } from "fastify"
import { Cursor } from "mongodb"
import { iterableToNdjsonStream } from "./iterable-to-ndjson-stream"
import { autoRefreshCursor as autoRefreshCursor } from "./auto-refresh-cursor"
import { distinctWith } from "./distinct-with"

const DEFAULT_PAGE_SIZE = 20

export async function streamOrPaginateSlow<T extends {}>(
  req: FastifyRequest<{ Querystring: { after?: number } }>,
  reply: FastifyReply,
  {
    pageSize = DEFAULT_PAGE_SIZE,
    getResults,
    idSelector,
  }: {
    getResults: () => Cursor<T>
    pageSize?: number
    idSelector: (doc: T) => unknown
  }
) {
  if (req.headers.accept === "application/x-ndjson") {
    reply.header("content-type", "application/x-ndjson")
    reply.send(
      iterableToNdjsonStream(
        distinctWith(
          autoRefreshCursor(() => getResults()),
          idSelector
        )
      )
    )
  } else {
    const results = await getResults()
      .skip(req.query.after ?? 0)
      .limit(pageSize + 1)
      .toArray()

    const hasNext = results.length === pageSize + 1

    return {
      items: results.slice(0, pageSize),
      hasNext,
      nextPage: hasNext ? (req.query.after ?? 0) + pageSize : undefined,
    }
  }
}
