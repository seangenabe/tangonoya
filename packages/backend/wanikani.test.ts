import anyTest, { TestInterface } from "ava"
import { createServer, Server } from "."

const test = anyTest as TestInterface<{ server: Server }>

test.beforeEach(async (t) => {
  t.context.server = await createServer({ logger: false })
  await t.context.server.ready()
})

test("entries/:id", async (t) => {
  const id = 5
  const { _id, ...entry } = await t.context.server.mongo.client
    .db()
    .collection("wanikani")
    .findOne({ _id: id })

  const response = await t.context.server.inject({
    method: "GET",
    path: `/wanikani/entries/${id}`,
  })

  const body = JSON.parse(response.body)

  t.deepEqual(entry, body)
})
