import { FastifyReply, FastifyRequest } from "fastify"
import { Cursor } from "mongodb"
import { iterableToNdjsonStream } from "./iterable-to-ndjson-stream"

export async function streamOrGetAllResults<T>(
  req: FastifyRequest,
  reply: FastifyReply,
  results: Cursor<T>
) {
  if (req.headers.accept === "application/x-ndjson") {
    reply.header("content-type", "application/x-ndjson")
    reply.send(iterableToNdjsonStream(results))
  } else {
    const resultsArray = await results.toArray()
    return resultsArray
  }
}
