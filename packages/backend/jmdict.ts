import {
  any,
  array,
  boolean,
  buildConst,
  integer,
  object,
  optional,
  string,
  stringEnum,
} from "@schema-kit/common"
import { toJsonSchema } from "@schema-kit/json-schema"
import { ToTypescriptType } from "@schema-kit/typescript"
import { paths } from "@tangonoya/common"
import {
  JmdictMeta,
  JmdictPatch,
  JmdictSearchEntry,
} from "@tangonoya/provider-jmdict"
import {
  fieldValues,
  jmdictJsonSchema,
  jmdictSchema,
  keInfValues,
  keRePriValues,
  miscValues,
  posValues,
  reInfValues,
} from "@tangonoya/provider-jmdict/types"
import { Jmdict } from "@tangonoya/provider-jmdict/types.gen"
import escapeStringRegexp from "escape-string-regexp"
import { FastifyPluginAsync } from "fastify"
import { readFile } from "fs/promises"
import { from, toArray } from "ix/asynciterable"
import { buffer, concatAll, map, skip, take } from "ix/asynciterable/operators"
import { Index, tokenizer } from "lunr"
import { createMatcher } from "./create-matcher"
import { iterableToNdjsonStream } from "./iterable-to-ndjson-stream"
import { streamOrGetAllResults } from "./stream-or-get-all-results"
import {
  paginatedSchema,
  streamOrPaginateResults,
} from "./stream-or-paginate-results"
import { streamOrPaginateSlow } from "./stream-or-paginate-slow"

export const ERR_NOTFOUND = "Entry not found."
const HTTP_OK = 200
const HTTP_NOTFOUND = 404

export const jmdict: FastifyPluginAsync = async (server) => {
  const db = server.mongo.client.db()
  const entriesCollection = db?.collection<Jmdict & { _id: number }>("jmdict")
  const searchCollection = db?.collection<JmdictSearchEntry>("jmdict-search")
  const patchesCollection = db?.collection<JmdictPatch>("jmdict-patches")
  const metaCollection = db?.collection<JmdictMeta>("meta")
  const indexesByLanguage = new Map<string, Index>()

  if (entriesCollection == null) {
    return
  }

  const languages = ["eng"] as const

  for (const lang of languages) {
    const indexStr = await readFile(
      `${paths.data}/jmdict/gloss-index-${lang}.json`,
      { encoding: "utf8" }
    )
    const index = Index.load(JSON.parse(indexStr))
    indexesByLanguage.set(lang, index)
  }

  const entryParamsSchema = object({
    id: integer({
      description: "The `ent_seq` filed of the entry.",
      minimum: 0,
    }),
  })

  server.get<{ Params: ToTypescriptType<typeof entryParamsSchema> }>(
    "/entries/:id",
    {
      schema: {
        description: "Gets a JMdict entry with the specified ID.",
        params: toJsonSchema(entryParamsSchema),

        response: {
          [HTTP_NOTFOUND]: toJsonSchema(
            buildConst(ERR_NOTFOUND, {
              description: "The entry with the specified ID does not exist.",
            })
          ),
          [HTTP_OK]: jmdictJsonSchema,
        },
      },
    },
    async (req, reply) => {
      const entry = await entriesCollection.findOne({
        _id: req.params.id,
      })

      if (entry == null) {
        reply.code(HTTP_NOTFOUND)
        return ERR_NOTFOUND
      }

      return entry
    }
  )

  const entriesQuerystringSchema = object({
    after: optional(
      integer({
        description: "Used for pagination.",
        default: 0,
        examples: [0],
      })
    ),
    keb_reb_starts: optional(
      string({
        description: "Filter entries by starting reb or keb value.",
      })
    ),
    ke_inf: optional(
      array(keInfValues, {
        description: "Filter entries by ke_inf value.",
        raw: {
          collectionFormat: "multi",
        },
      })
    ),
    re_inf: optional(
      array(reInfValues, {
        description: "Filter entries by re_inf value.",
        raw: {
          collectionFormat: "multi",
        },
      })
    ),
    ke_re_pri: optional(
      array(keRePriValues, {
        description: "Filter entries by ke_pri and re_pri value.",
        raw: {
          collectionFormat: "multi",
        },
      })
    ),
    re_nokanji: optional(
      boolean({ description: "Filter entries by re_nokanji value." })
    ),
    pos: optional(
      array(posValues, {
        description: "Filter entries by pos value.",
        raw: {
          collectionFormat: "multi",
        },
      })
    ),
    field: optional(
      array(fieldValues, {
        description: "Filter entries by application/field of entry.",
        raw: {
          collectionFormat: "multi",
        },
      })
    ),
    misc: optional(
      array(miscValues, {
        description: "Filter entries by misc field value.",
        raw: {
          collectionFormat: "multi",
        },
      })
    ),
  })

  server.get<{
    Querystring: ToTypescriptType<typeof entriesQuerystringSchema>
  }>(
    "/entries",
    {
      schema: {
        description: "Gets all JMdict entries.",
        response: {
          [HTTP_OK]: toJsonSchema(paginatedSchema(jmdictSchema)),
        },
        querystring: toJsonSchema(entriesQuerystringSchema),
      },
    },
    async (req, reply) => {
      const meta = await metaCollection.findOne({ _id: "jmdict" })
      const { mdate } = meta!
      reply.header("x-jmdict-mdate", mdate)
      reply.header("access-control-expose-headers", "x-jmdict-mdate")

      if (Object.keys(req.query).length === 0) {
        // Just return everything.
        return await streamOrPaginateResults(
          req,
          reply,
          (_id?: number) => entriesCollection.find({ _id: { $gt: _id } }),
          {
            pageSize: 20,
          }
        )
      }

      const getResults = () => {
        const commands: object[] = []

        if (req.query.keb_reb_starts) {
          commands.unshift({
            $match: {
              $or: [
                {
                  "k_ele.keb": {
                    $regex: new RegExp(
                      `^${escapeStringRegexp(req.query.keb_reb_starts)}`,
                      "i"
                    ),
                  },
                },
                {
                  "r_ele.reb": new RegExp(
                    `^${escapeStringRegexp(req.query.keb_reb_starts)}`,
                    "i"
                  ),
                },
              ],
            },
          })
        }
        if (req.query.ke_inf) {
          commands.push({
            $match: { "k_ele.ke_inf": { $in: req.query.ke_inf } },
          })
        }
        if (req.query.re_inf) {
          commands.push({
            $match: { "r_ele.re_inf": { $in: req.query.re_inf } },
          })
        }
        if (req.query.ke_re_pri) {
          commands.push({
            $match: {
              $or: [
                { "k_ele.ke_pri": { $in: req.query.ke_re_pri } },
                { "r_ele.re_pri": { $in: req.query.ke_re_pri } },
              ],
            },
          })
        }
        if (req.query.re_nokanji != null) {
          commands.push({
            $match: {
              "r_ele.re_nokanji": { $exists: req.query.re_nokanji },
            },
          })
        }
        if (req.query.pos) {
          commands.push({
            $match: { "sense.pos": { $in: req.query.pos } },
          })
        }
        if (req.query.field) {
          commands.push({
            $match: {
              "sense.field": { $in: req.query.field },
            },
          })
        }
        if (req.query.misc) {
          commands.push({
            $match: {
              "sense.misc": { $in: req.query.misc },
            },
          })
        }

        const cursor = entriesCollection.aggregate(commands)

        return cursor
      }

      return await streamOrPaginateSlow(req, reply, {
        getResults,
        idSelector: ({ _id }) => _id,
      })
    }
  )

  const searchQuerystringSchema = object({
    after: optional(
      integer({
        description: "Used for pagination.",
        default: 0,
        examples: [0],
      })
    ),
    q: string({
      description: "Filter entries.",
    }),
    search_mode: stringEnum(["exact", "prefix"] as const),
  })

  server.get<{
    Querystring: ToTypescriptType<typeof searchQuerystringSchema>
  }>(
    "/search",
    {
      schema: {
        description:
          "Search-optimized version of `entries`. Gets all matching JMdict entries.",
        response: {
          [HTTP_OK]: toJsonSchema(paginatedSchema(jmdictSchema)),
        },
        querystring: toJsonSchema(searchQuerystringSchema),
      },
    },
    async (req, reply) => {
      const { mdate } = (await metaCollection.findOne({ _id: "jmdict" }))!

      reply.header("x-mdate", mdate)
      reply.header("access-control-expose-headers", "x-mdate")

      const getResults = () => {
        return searchCollection.aggregate<Jmdict & { _id: number }>([
          {
            $match: {
              h: createMatcher(req.query.q, req.query.search_mode),
            },
          },
          {
            $lookup: {
              from: "jmdict",
              localField: "e",
              foreignField: "_id",
              as: "jmdictEntry",
            },
          },
          {
            $unwind: {
              path: "$jmdictEntry",
              preserveNullAndEmptyArrays: false,
            },
          },
          {
            $replaceRoot: {
              newRoot: "$jmdictEntry",
            },
          },
        ])
      }

      return await streamOrPaginateSlow(req, reply, {
        getResults,
        idSelector: ({ _id }) => _id,
      })
    }
  )

  const searchGlossesSchema = object({
    after: optional(
      integer({
        description: "Used for pagination.",
        default: 0,
        examples: [0],
      })
    ),
    q: string({
      description: "Filter entries.",
    }),
    lang: stringEnum(languages),
  })

  server.get<{ Querystring: ToTypescriptType<typeof searchGlossesSchema> }>(
    "/search-glosses",
    {
      schema: {
        description: "Search entries from dictionary glosses.",
        response: {
          [HTTP_OK]: toJsonSchema(paginatedSchema(jmdictSchema)),
        },
        querystring: toJsonSchema(searchGlossesSchema),
      },
    },
    async (req, reply) => {
      const { mdate } = (await metaCollection.findOne({ _id: "jmdict" }))!

      reply.header("x-mdate", mdate)
      reply.header("access-control-expose-headers", "x-mdate")

      const index = indexesByLanguage.get(req.query.lang)!
      const indexResults = index.query((query) => {
        const tokens = tokenizer(req.query.q)
        for (const token of tokens) {
          query.term(token, {
            fields: ["glosses"],
            // wildcard: Query.wildcard.TRAILING,
          })
        }
      })

      const getEntriesFromIds = async (ids: number[]) => {
        if (ids.length === 0) {
          return []
        }
        const entries = await entriesCollection
          .find({
            _id: {
              $in: ids,
            },
          })
          .toArray()
        const entriesById = new Map(
          entries.map((entry) => [entry.ent_seq, entry])
        )

        return ids.map((id) => entriesById.get(id)!)
      }

      if (req.headers.accept === "application/x-ndjson") {
        reply.header("content-type", "application/x-ndjson")

        const source = from(indexResults).pipe(
          map((r) => Number(r.ref)),
          buffer(20),
          map(getEntriesFromIds),
          concatAll()
        )

        reply.send(iterableToNdjsonStream(source))
        return
      }
      const after = req.query.after ?? 0
      const ids = await toArray(
        from(indexResults).pipe(
          skip(after),
          take(20),
          map((r) => Number(r.ref))
        )
      )
      const results = await getEntriesFromIds(ids)
      const hasNext = results.length === 20

      return {
        items: results,
        hasNext,
        nextPage: after + results.length,
      }
    }
  )

  const patchesQueryStringSchema = object({
    after: string({
      description: "The exclusive lower bound for filtering patches.",
    }),
  })

  const patchSchema = object({
    mdate: string(),
    ent_seq: integer(),
    operations: optional(array(any({ title: "Operation" }))),
    newDocument: optional(jmdictSchema),
  })

  server.get<{
    Querystring: ToTypescriptType<typeof patchesQueryStringSchema>
  }>(
    "/patches",
    {
      schema: {
        description:
          "Gets all changes to JMdict entries after the specified date.",
        querystring: toJsonSchema(patchesQueryStringSchema),
        response: {
          [HTTP_OK]: toJsonSchema(array(patchSchema)),
        },
      },
    },
    async (req, reply) => {
      const results = patchesCollection
        .find({
          mdate: { $gt: req.query.after },
        })
        .sort([["mdate", 1]])

      return await streamOrGetAllResults(req, reply, results)
    }
  )

  const statsSchema = object({
    totalCount: integer(),
    mdate: string(),
  })

  server.get(
    "/stats",
    {
      schema: {
        response: {
          [HTTP_OK]: toJsonSchema(statsSchema),
        },
      },
    },
    async () => {
      const meta = await metaCollection.findOne({ _id: "jmdict" })

      return {
        mdate: meta!.mdate,
        totalCount: await entriesCollection.countDocuments(),
      }
    }
  )

  const languagesSchema = array(
    object({
      code: string(),
      description: string(),
    })
  )

  server.get(
    "/languages",
    {
      schema: {
        response: {
          [HTTP_OK]: toJsonSchema(languagesSchema),
        },
      },
    },
    async () => {
      const meta = await metaCollection.findOne({ _id: "jmdict" })

      return meta?.languages ?? []
    }
  )
  ;(async () => {
    await server.ready()
    await entriesCollection.createIndexes(
      [
        { "k_ele.keb": 1 },
        { "r_ele.reb": 1 },
        { "k_ele.ke_pri": 1 },
        { "r_ele.re_pri": 1 },
        { "r_ele.re_nokanji": 1 },
        { "sense.pos": 1 },
        { "sense.field": 1 },
        { "sense.misc": 1 },
      ].map((key) => ({ key }))
    )
    await patchesCollection.createIndex({ mdate: 1, _id: 1 })
  })()
}
