export async function* distinctWith<T, Key = unknown>(
  source: AsyncIterable<T>,
  keySelector: (element: T) => Key
) {
  const seenKeys = new Set<Key>()

  for await (const element of source) {
    const key = keySelector(element)
    if (seenKeys.has(key)) {
      continue
    }

    seenKeys.add(key)
    yield element
  }
}
