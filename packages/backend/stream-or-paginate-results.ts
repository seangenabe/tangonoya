import {
  array,
  boolean,
  number,
  object,
  optional,
  Schema,
} from "@schema-kit/common"
import { FastifyRequest, FastifyReply } from "fastify"
import { Cursor } from "mongodb"
import { iterableToNdjsonStream } from "./iterable-to-ndjson-stream"

const DEFAULT_PAGE_SIZE = 20

export async function streamOrPaginateResults<
  ID extends string | number = string | number,
  SortBy extends string = "_id",
  T extends { [key in SortBy]: ID } = { [key in SortBy]: ID }
>(
  req: FastifyRequest<{ Querystring: { after?: ID } }>,
  reply: FastifyReply,
  getResults: (idCursor?: ID) => Cursor<T>,
  {
    sortBy = "_id" as SortBy,
    pageSize = DEFAULT_PAGE_SIZE,
  }: {
    sortBy?: SortBy
    pageSize?: number
  } = {}
) {
  if (req.headers.accept === "application/x-ndjson") {
    reply.header("content-type", "application/x-ndjson")
    reply.send(iterableToNdjsonStream(getResults()))
  } else {
    const results = await getResults(req.query.after)
      .sort({ [sortBy]: 1 })
      .limit(pageSize + 1)
      .toArray()
    return {
      items: results.slice(0, pageSize),
      hasNext: results[pageSize] != null,
      nextPage: results[pageSize - 1]?.[sortBy],
    }
  }
}

export function paginatedSchema(itemSchema: Schema) {
  return object({
    items: array(itemSchema),
    hasNext: boolean(),
    nextPage: optional(number()),
  })
}
