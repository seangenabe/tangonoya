import { PassThrough } from "stream"

export function iterableToNdjsonStream<T>(items: AsyncIterable<T>) {
  const stream = new PassThrough()
  ;(async () => {
    try {
      for await (const item of items) {
        await new Promise((resolve) =>
          stream.write(JSON.stringify(item) + "\n", "utf-8", resolve)
        )
      }
      stream.end()
    } catch (err) {
      stream.emit("error", err)
    }
  })()
  return stream
}
