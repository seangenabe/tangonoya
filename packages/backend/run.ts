import "source-map-support/register"

import { createServer } from "./"
// @ts-ignore
import FastifyGs from "fastify-graceful-shutdown"
;(async () => {
  try {
    const server = await createServer()
    server.register(FastifyGs)
    server.listen(process.env.PORT || 2000, "0.0.0.0")
  } catch (err) {
    console.error(err)
    process.exit(1)
  }
})()
