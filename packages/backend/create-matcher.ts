import escapeStringRegexp from "escape-string-regexp"

export function createMatcher(q: string, searchMode: "exact" | "prefix") {
  if (searchMode === "exact") {
    return q
  } else {
    return new RegExp(`^${escapeStringRegexp(q)}.+`)
  }
}
