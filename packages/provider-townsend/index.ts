import { createReadStream } from "fs"
import { createInterface } from "readline"
import { CommonDatabaseUpdateOpts } from "@tangonoya/common"

export async function updateDatabase({ collection }: CommonDatabaseUpdateOpts) {
  for await (let pg of getPhoneticGroups()) {
    await collection.updateOne(
      { _id: pg.component },
      { $set: pg },
      { upsert: true }
    )
  }
}

export async function* getPhoneticGroups(): AsyncIterableIterator<PhoneticGroup> {
  const stream = createReadStream(`${__dirname}/kanji.tsv`)
  const rl = createInterface({ input: stream, crlfDelay: Infinity })
  for await (let line of rl) {
    if (line === "") continue
    const [component, commonReadingsRaw, kanjisRaw] = line.split("\t")
    yield {
      component,
      commonReadings: commonReadingsRaw.split(","),
      kanjis: kanjisRaw.split(","),
    }
  }
}

export interface PhoneticGroup {
  component: string
  commonReadings: string[]
  kanjis: string[]
}
