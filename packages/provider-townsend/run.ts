#!/usr/bin/env node

import { getConfig } from "@tangonoya/common"
import { connect } from "mongodb"
import { updateDatabase } from "."

export async function runUpdateDatabase() {
  const config = getConfig()

  const client = await connect(config.db)

  try {
    const db = client.db()
    const collection = db.collection("townsend")
    const metaCollection = db.collection("meta")

    await client.withSession(async (session) => {
      await updateDatabase({
        session,
        collection,
        metaCollection,
        logger: { enabled: true },
        transactionOptions: {
          readConcern: { level: "majority" },
          writeConcern: { w: "majority" },
        },
      })
    })
  } finally {
    await client.close()
  }
}

if (require.main === module) {
  runUpdateDatabase().catch((err) => {
    console.error(err)
    process.exit(1)
  })
}
