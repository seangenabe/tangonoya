# tangonoya-provider-townsend

Tangonoya provider for list of kanji by phonetic components by Hiroko Townsend

Source:
* https://www.tofugu.com/japanese/look-up-kanji/
* http://sdsu-dspace.calstate.edu/bitstream/handle/10211.10/1203/Townsend_Hiroko.pdf?sequence=1
