import { ClientSession, Collection, TransactionOptions } from "mongodb"
import { LoggerOptions } from "pino"
import rc from "rc"
import { PassThrough } from "stream"

export function omitNullOrUndefined<T extends {}>(
  obj: T
): Pick<
  T,
  {
    [key in keyof T]: T[key] extends undefined | null ? never : key
  }[keyof T]
> {
  const ret: any = {}
  for (let [key, value] of Object.entries(obj) as [keyof T, T[keyof T]][]) {
    if (value != null) {
      ret[key] = obj[key] as any
    }
  }
  return ret
}

const defaultConfig: Partial<Config> = {
  db: "mongodb://localhost:27017/tangonoya?replicaSet=rs0",
}

export function _mergeDefaultConfig(config: Partial<Config>) {
  const keys1 = new Set(Object.keys(defaultConfig))
  const keys2 = Object.keys(config)

  for (let k2 of keys2) {
    if (keys1.has(k2)) {
      throw new Error(`Key conflict: ${k2}`)
    }
  }

  Object.assign(defaultConfig, config)
}

export function getConfig() {
  const conf = rc("tangonoya", defaultConfig)
  return conf as Config
}

export function iterableToNdjsonStream<T>(items: AsyncIterable<T>) {
  const stream = new PassThrough()
  ;(async () => {
    try {
      for await (const item of items) {
        await new Promise((resolve) =>
          stream.write(JSON.stringify(item) + "\n", "utf-8", resolve)
        )
      }
      stream.end()
    } catch (err) {
      stream.emit("error", err)
    }
  })()
  return stream
}

export interface CommonDatabaseUpdateOpts<
  Item = unknown,
  MetaValue = unknown,
  Other extends {} = {}
> {
  session: ClientSession
  collection: Collection<Item & Other>
  metaCollection: Collection<{ _id: string } & MetaValue>
  logger?: LoggerOptions
  transactionOptions?: TransactionOptions
}

export interface Config {
  db: string
}

export { paths } from "./paths"
