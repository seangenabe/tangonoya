import envPaths from "env-paths"

export const paths = envPaths("tangonoya")

if (require.main === module) {
  process.stdout.write(JSON.stringify(paths))
}
