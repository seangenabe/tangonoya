# Tangonoya

Data source for Japanese words and kanji.

## Usage

### JMdict updater

```typescript
import { updateDatabase } from "@tangonoya/provider-jmdict"
```

#### updateDatabase(opts = {}): Promise<void>

Updates a MongoDB database with the latest entries from JMdict.

* `opts.gunzipUrl` - Passed to `gunzip-then-rsync`
* `opts.rsyncSource` - Passed to `gunzip-then-rsync`

### WaniKani updater

```typescript
import { updateDatabase } from "@tangonoya/provider-wanikani"
```

#### updateDatabase(opts): Promise<void>

Updates a MongoDB database with 'subjects' from the WaniKani API.

* `opts.apiKey` - A WaniKani v2 API key.

### Townsend updater

```typescript
import { updateDatabase } from "@tangonoya/provider-townsend"
```

#### updateDatabase(opts): Promise<void>

Update a MongoDB database with Townsend's phonetic components data.

## License

This source code itself is licensed with MIT.
However, source files may be licensed under different terms:

* The JMdict is under the [EDRDG General Dictionary Licence Statement](http://www.edrdg.org/edrdg/licence.html).
* Usage of the WaniKani API is covered by the [WaniKani Terms of Service](https://www.wanikani.com/terms)
